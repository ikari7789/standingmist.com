<?php

ob_implicit_flush(false);
ob_start();
session_start();
require_once 'lib/session.php';
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['year'])) {
        define('TITLE', 'Posts - '.$_GET['year']);
    } elseif (isset($_GET['page'])) {
        if ($_GET['page'] == 'archive') {
            define('TITLE', 'Post Archive');
        } elseif ($_GET['page'] == 'gallery') {
            define('TITLE', 'Gallery');
        } elseif ($_GET['page'] == 'japanese') {
            define('TITLE', 'Japanese');
        } elseif ($_GET['page'] == 'portfolio') {
            define('TITLE', 'Portfolio');
        } elseif ($_GET['page'] == 'register') {
            define('TITLE', 'Register');
        } elseif ($_GET['page'] == 'contact') {
            define('TITLE', 'Contact');
        } elseif (is_numeric($_GET['page'])) {
            define('TITLE', 'Posts - '.$_GET['page']);
        }
    } elseif (isset($_GET['post'])) {
        define('TITLE', preg_replace('#-#is', ' ', $_GET['post']));
    } else {
        define('TITLE', 'Standing in the Mist - 霞で立ってる');
    }
}
require_once 'inc/header.php';

echo '<body>';
require_once 'inc/banner.php';
require_once 'inc/body.php';
require_once 'inc/footer.php';
echo '</body>';
echo '</html>';

ob_end_flush();
