<?php

class Calendar
{
    private $db;

    private $monthNum = [
        'January' => 1,
        'February' => 2,
        'March' => 3,
        'April' => 4,
        'May' => 5,
        'June' => 6,
        'July' => 7,
        'August' => 8,
        'September' => 9,
        'October' => 10,
        'November' => 11,
        'December' => 12,
    ];

    private $monthName = [
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December',
    ];

    public function __construct($month = null, $year = null)
    {
        global $db;
        $this->db = $db;
        $date = getdate();
        if (! is_null($month) && ! is_null($year)) {
            if (is_numeric($month)) {
                $month = $this->monthName[(int) $month];
            }
            $this->draw($month, $year);
        } else {
            $this->draw($date['month'], $date['year']);
        }
    }

    public function draw($month, $year)
    {
        $firstDay = $this->getFirstDay($month, $year);

        if (is_int($year / 400) || ! is_int($year / 100) && is_int($year / 4)) {
            $febDays = 29;
        } else {
            $febDays = 28;
        }

        $daysPerMonth = [
            1 => 31,
            2 => $febDays,
            3 => 31,
            4 => 30,
            5 => 31,
            6 => 30,
            7 => 31,
            8 => 31,
            9 => 30,
            10 => 31,
            11 => 30,
            12 => 31,
        ];

        $currMonthDays = $daysPerMonth[$this->monthNum[$month]];
        if ($this->monthNum[$month] == 1) {
            $prevMonth = 12;
        } else {
            $prevMonth = $this->monthNum[$month] - 1;
        }
        $prevMonthDays = $daysPerMonth[$prevMonth];

        switch ($firstDay) {
            case 'Sunday':
                $prevMonthSpacer = 7;
                break;
            case 'Monday':
                $prevMonthSpacer = 1;
                break;
            case 'Tuesday':
                $prevMonthSpacer = 2;
                break;
            case 'Wednesday':
                $prevMonthSpacer = 3;
                break;
            case 'Thursday':
                $prevMonthSpacer = 4;
                break;
            case 'Friday':
                $prevMonthSpacer = 5;
                break;
            case 'Saturday':
                $prevMonthSpacer = 6;
                break;
        }

        $calendarDays = [];

        $counter = 0;
        for ($i = $prevMonthDays - $prevMonthSpacer; $i <= $prevMonthDays; $i++) {
            $calendarDays[$counter] = $i;
            $counter++;
        }
        for ($i = 1; $i <= $currMonthDays; $i++) {
            $calendarDays[$counter] = $i;
            $counter++;
        }
        $newMonth = 1;
        while (count($calendarDays) <= 42) {
            $calendarDays[$counter] = $newMonth;
            $newMonth++;
            $counter++;
        }

        $linkMonth = $this->monthNum[$month];
        if ($this->monthNum[$month] < 10) {
            $linkMonth = '0'.$linkMonth;
        }
        ?>
<script type="text/javascript" src="/js/calendar.js"></script>

<div class="calendar">
    <div class="cal_title">
        <a href="/" class="prevMonth">&lt;&lt;&lt;</a>
        <span class="month">
            <a href="/<?php echo $year;
        ?>/<?php echo $linkMonth;
        ?>"><?php echo $month;
        ?></a>
        </span>
        <a href="/" class="nextMonth">&gt;&gt;&gt;</a>
    </div>
    <div class="cal_year" id="year"><a href="/<?php echo $year;
        ?>"><?php echo $year;
        ?></a></div>
    <table class="cal_nums">
        <thead class="cal_days">
            <tr>
                <th>Su</th>
                <th>Mo</th>
                <th>Tu</th>
                <th>We</th>
                <th>Th</th>
                <th>Fr</th>
                <th>Sa</th>
            </tr>
        </thead>
        <tbody>
<?php
        reset($calendarDays);

        $postDays = $this->getPosts($month, $year);
        for ($row = 0; $row < 6; $row++) {
            echo '      <tr>'."\n";
            for ($column = 0; $column < 7; $column++) {
                $currDay = next($calendarDays);
                if ($currDay >= 23 && $row == 0) {
                    $class = 'prevMonth';
                } elseif ($currDay <= 21 && ($row == 4 || $row == 5)) {
                    $class = 'nextMonth';
                } else {
                    $class = 'currMonth';
                    if (is_array($postDays) && in_array($currDay, $postDays)) {
                        $linkDay = $currDay;
                        if ($currDay < 10) {
                            $linkDay = '0'.$linkDay;
                        }

                        $currDay = '<a href="/'.$year.'/'.$linkMonth.'/'.$linkDay.'">'.$currDay.'</a>';
                    }
                }

                echo "\t\t\t\t\t\t".'<td class="'.$class.'">'.$currDay.'</td>'."\n";
            }
            echo "\t\t\t\t\t".'</tr>'."\n";
        }
        echo "\t\t\t\t\t".'</tbody>'."\n".
            "\t\t\t\t".'</table>'."\n".
            "\t\t\t".'</div>'."\n";
    }

    public function getFirstDay($month, $year)
    {
        $first = getdate(strtotime("01 $month $year"));

        return $first['weekday'];
    }

    public function getPosts($month, $year)
    {
        $query = sprintf(
            'SELECT DAYOFMONTH(date) AS day FROM %sposts WHERE YEAR(date) = %d AND MONTHNAME(date) = "%s";',
            $this->db->escape($this->db->prefix),
            $year,
            $this->db->escape($month)
        );
        $result = $this->db->query($query);
        if (! is_bool($result)) {
            $days = [];
            while ($post = $this->db->fetchArray($result, MYSQLI_ASSOC)) {
                if (! in_array($post['day'], $days)) {
                    $days[] = $post['day'];
                }
            }

            return $days;
        } else {
            return;
        }
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['ajax']) && isset($_GET['month']) && isset($_GET['year'])) {
    include_once 'lib/constants.php';
    include_once 'lib/mysqli.php';
    new Calendar($_GET['month'], $_GET['year']);
}
