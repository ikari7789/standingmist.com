$(function() {
	var searchText = $('#search');
	searchText
		.val('Search...')
		.css('color', 'grey')
		.bind({
			focus: function() {
				if (searchText.val() == 'Search...') {
					searchText
						.val('')
						.css('color', 'black');
				}
			},
			blur: function() {
				$.unblockUI();
				if (searchText.val() == '') {
					searchText
						.val('Search...')
						.css('color', 'grey');
				}
			}
		});
	
	$('form#searchForm').bind('submit', function() {
		$.blockUI({
			message: '<h1>Searching...</h1>'
		});
		$.ajax({
			url: 'search.php?search=' + searchText.val(),
			cache: false,
			success: function(data) {
				var height = '225px';
				if ($(data).find('div.error').length > 0)
					height = '90px';
				$.blockUI({
					message: data,
					css: {
						top: '20%',
						left: '30%',
						right: '30%',
						width: '40%',
						height: height,
						centerX: true,
						centerY: true,
						textAlign: 'left',
						cursor: null
					},
					focusInput: true
				});
				$('.blockOverlay').attr('title','Click to unblock').click(function() {$.unblockUI; searchText.blur()});	
			}
		});	
		return false;
	});
	
	$('div.searchBox a.close').live('click', function() {
		$.unblockUI();
		return false;
	});
});