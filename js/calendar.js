$(function() {
	$('div.calendar a.prevMonth').bind('click', function() {
		var month = $('div.calendar span.month').text();
		var year = $('div.calendar div#year').text();
		
		switch (month) {
			case "January":
				month = "December";
				year--;
				break;
			case "February":
				month = "January";
				break;
			case "March":
				month = "February";
				break;
			case "April":
				month = "March";
				break;
			case "May":
				month = "April";
				break;
			case "June":
				month = "May";
				break;
			case "July":
				month = "June";
				break;
			case "August":
				month = "July";
				break;
			case "September":
				month = "August";
				break;
			case "October":
				month = "September";
				break;
			case "November":
				month = "October";
				break;
			case "December":
				month = "November";
				break;
		}
		url = 'calendar.php?ajax=1&month='+month+'&year='+year;
		
		$.ajax({
			url: url,
			success: function(data) {
				var newCalendar = $(data).find('table.cal_nums');
				$('div.calendar table.cal_nums').hide("slide", {direction: "right"}, '1000', function() {
					$('div.calendar table.cal_nums').html(newCalendar.html());
					$('div.calendar span.month').html($(data).find('span.month').html());
					$('div.calendar div#year').html($(data).find('div#year').html());
				}).show("slide", {direction: "left"}, '1000');
			}
		});
		
		return false;
	});

	$('div.calendar a.nextMonth').bind('click', function() {
		var month = $('div.calendar span.month').text();
		var year = $('div.calendar div#year').text();
		
		switch (month) {
			case "January":
				month = "February";
				break;
			case "February":
				month = "March";
				break;
			case "March":
				month = "April";
				break;
			case "April":
				month = "May";
				break;
			case "May":
				month = "June";
				break;
			case "June":
				month = "July";
				break;
			case "July":
				month = "August";
				break;
			case "August":
				month = "September";
				break;
			case "September":
				month = "October";
				break;
			case "October":
				month = "November";
				break;
			case "November":
				month = "December";
				break;
			case "December":
				month = "January";
				year++;
				break;
		}
		url = 'calendar.php?ajax=1&month='+month+'&year='+year;
		
		$.ajax({
			url: url,
			success: function(data) {
				var newCalendar = $(data).find('table.cal_nums');
				$('div.calendar table.cal_nums').hide("slide", {direction: "left"}, '1000', function() {
					$('div.calendar table.cal_nums').html(newCalendar.html());
					$('div.calendar span.month').html($(data).find('span.month').html());
					$('div.calendar div#year').html($(data).find('div#year').html());
				}).show("slide", {direction: "right"}, '1000');
			}
		});
		
		return false;
	});
});