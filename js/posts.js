// Replaces the currently selected text with the passed text.
function replaceText(text, textarea) {
	// Attempt to create a text range (IE).
	if (typeof(textarea.caretPos) != "undefined" && textarea.createTextRange) {
		var caretPos = textarea.caretPos;

		caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) == ' ' ? text + ' ' : text;
		caretPos.select();
	}
	// Mozilla text range replace.
	else if (typeof(textarea.selectionStart) != "undefined") {
		var begin = textarea.value.substr(0, textarea.selectionStart);
		var end = textarea.value.substr(textarea.selectionEnd);
		var scrollPos = textarea.scrollTop;

		textarea.value = begin + text + end;

		if (textarea.setSelectionRange) {
			textarea.focus();
			textarea.setSelectionRange(begin.length + text.length, begin.length + text.length);
		}
		textarea.scrollTop = scrollPos;
	}
	// Just put it on the end.
	else
	{
		textarea.value += text;
		textarea.focus(textarea.value.length - 1);
	}
}

// Surrounds the selected text with text1 and text2.
function surroundText(text1, text2, textarea) {
	// Can a text range be created?
	if (typeof(textarea.caretPos) != "undefined" && textarea.createTextRange) {
		var caretPos = textarea.caretPos, temp_length = caretPos.text.length;

		caretPos.text = caretPos.text.charAt(caretPos.text.length - 1) == ' ' ? text1 + caretPos.text + text2 + ' ' : text1 + caretPos.text + text2;

		if (temp_length == 0) {
			caretPos.moveStart("character", -text2.length);
			caretPos.moveEnd("character", -text2.length);
			caretPos.select();
		}
		else
			textarea.focus(caretPos);
	}
	// Mozilla text range wrap.
	else if (typeof(textarea.selectionStart) != "undefined") {
		var begin = textarea.value.substr(0, textarea.selectionStart);
		var selection = textarea.value.substr(textarea.selectionStart, textarea.selectionEnd - textarea.selectionStart);
		var end = textarea.value.substr(textarea.selectionEnd);
		var newCursorPos = textarea.selectionStart;
		var scrollPos = textarea.scrollTop;

		textarea.value = begin + text1 + selection + text2 + end;

		if (textarea.setSelectionRange) {
			if (selection.length == 0)
				textarea.setSelectionRange(newCursorPos + text1.length, newCursorPos + text1.length);
			else
				textarea.setSelectionRange(newCursorPos, newCursorPos + text1.length + selection.length + text2.length);
			textarea.focus();
		}
		textarea.scrollTop = scrollPos;
	}
	// Just put them on the end, then.
	else {
		textarea.value += text1 + text2;
		textarea.focus(textarea.value.length - 1);
	}
}

$(function() {
	var tempData = new Array();
	var imgsrc = new Array();
	var publicKey = '6LdHNLwSAAAAAF8m8ccQWgZznlJw1LkidCMONCY5';

	// Comment functions

	// Click the "Add a Comment" link
	$('a#addComment').live('click', function() {
		if (!$(this).hasClass('clicked')) {
			$(this).addClass('clicked');
			var url = 'comment';

			newComment = $(this);

			$.ajax({
				url: url+'?newComment=1',
				dataType: 'html',
				cache: false,
				success: function(data) {
					newComment.after(data);
					newComment.hide();
					$('div.inner-box').hide();
					$('div.inner-box img').each(function(index) {
						imgsrc[index] = $(this).attr('src');
						$(this).attr('src', '');
					});
					$(this).removeClass('clicked');
				}
			});
		}
		return false;
	});

	// Click the [X] button to cancel
	$('form#edit-form.comment-form a.cancel').live('click', function() {
		var parent = $(this).parent();
		parent.remove();
		$('a#addComment').show().removeClass('clicked');
		return false;
	});

	function getComment(form) {
		// form elements to check for errors
		var comment = {
			name: form.find('input#name'),
			email: form.find('input#email'),
			homepage: form.find('input#homepage'),
			content: form.find('textarea#content'),
			nameError: form.find('input#name').parent().find('span.error'),
			emailError: form.find('input#email').parent().find('span.error'),
			contentError: form.find('textarea#content').parent().find('span.error')
		}

		return comment;
	}

	function checkName(name, error) {
		if (name.val() == '') {
			error.text('Must have a name.');
			return 1;
		} else if (name.val().length < 3) {
			error.text('Name must be longer than 2 characters.');
			return 1;
		} else if (name.val().length > 32) {
			error.text('Name must be shorter than 33 characters.');
			return 1;
		} else {
			error.text('');
			return 0;
		}
	}

	function checkEmail(email, error) {
		var validRegExp = /^[^@]+@[^@]+.[a-z]{2,}$/i;

		if (email.val() == '') {
			error.text('Must have an email');
			return 1;
		} else if (email.val().search(validRegExp) == -1) {
			error.text('Must use a valid email address');
			return 1;
		} else {
			error.text('');
			return 0;
		}
	}

	function checkContent(content, error) {
		if (content.val() == '') {
			error.text('Must have a comment');
			return 1;
		} else {
			error.text('');
			return 0;
		}
	}

	// Commit comment to database
	function commitComment(comment) {
		alert('adding comment:\n'+comment.name.val()+'\n'+comment.email.val()+'\n'+comment.homepage.val()+'\n'+comment.content.val());
	}

	// Click the "Add Comment" button to add a comment
	$('form#edit-form.comment-form #process').live('click', function() {
		$(this).attr('disabled', 'disabled');
		var commentForm = $(this).parents('form#edit-form.comment-form');

		// form elements to check for errors
		var comment = getComment(commentForm);

		var errors = 0;
		errors += checkName(comment.name, comment.nameError);
		errors += checkEmail(comment.email, comment.emailError);
		errors += checkContent(comment.content, comment.contentError);

		if (errors == 0) {
			if ($(commentForm).find('input#loggedIn').val() == 'true') {
				$.ajax({
					url: 'verify',
					type: 'POST',
					data: commentForm.serialize()+'&postTitle='+$('div.post-title').attr('id'),
					success: function(data) {
						$('form#edit-form.comment-form a.cancel').click();
						$('div#newComment').after(data);
						//alert(data);
					}
				});
			} else {

			}
		}
		return false;
	});

	// Post functions
	// Click the "Add a Post" link
	$('a.addPostLink').live('click', function() {
		if (!$(this).hasClass('clicked')) {
			$(this).addClass('clicked');
			var url = 'edit/new-post';

			$.ajax({
				url: url,
				success: function(data) {
					var newPost = $('div#newPost');
					tempData[0] = newPost.html();
					newPost.fadeTo("normal", 0.0, function() {
						$(this).wrap('<div class="tempContainer"></div>');
						$(data).insertBefore(newPost);
						$('div.inner-box').hide();
						$('div.inner-box img').each(function(index) {
							imgsrc[index] = $(this).attr('src');
							$(this).attr('src', '');
						});
						$(this).hide();
						postContent = document.getElementById('content');
						$(this).removeClass('clicked');
					}).fadeTo("normal", 1.0);
				}
			});
		}
		return false;
	});

	// Click the "Edit Post" link
	$('a.editPost').live('click', function() {
		if (!$(this).hasClass('clicked')) {
			$(this).addClass('clicked');
			var url = $(this).attr('href');
			var post = $(this).parents('div.box');

			$.ajax({
				url: url,
				success: function(data) {
					var postID = post.attr('id').substr(5);
					post.fadeTo("normal", 0.0, function() {
						$(this).wrap('<div class="tempContainer"></div>');
						$(data).insertBefore(post);
						$('div.inner-box').hide();
						$('div.inner-box img').each(function(index) {
							imgsrc[index] = $(this).attr('src');
							$(this).attr('src', '');
						});
						$(this).hide();
					}).fadeTo("normal", 1.0);
				}
			});
		}

		return false;
	});

	// Click the [X] to cancel editing or adding a new post
	$('form#edit-form.edit-form a.cancel').live('click', function() {
		var parent = $(this).parents('div.tempContainer');
		parent.find('a.addPostLink').removeClass('clicked');
		parent.find('a.editPost').removeClass('clicked');
		parent.find('div#previewBox').remove();
		parent.find('div#editBox').remove();
		parent.children().show().unwrap();
		return false;
	});

	// Click the "Preview" button to get a preview of the post
	$('form#edit-form.edit-form button#preview').live('click', function() {

		form = $('form#edit-form');
		parent = $(this).closest('div.tempContainer');
		//alert(form.serialize());
		$.ajax({
			type: 'POST',
			url: 'edit.php',
			data: 'preview=1&'+form.serialize(),
			success: function(data) {
				parent.find("#previewBox").html("<h1><i>Preview:</i></h1>\n<hr style=\"border: 1px dashed;\"/>\n" + data + "\n").show();
			}
		});
		return false;
	});

	// Click the "Add" button to add a post to the database
	$('form#edit-form.edit-form button#process').live('click', function() {
		var url = 'edit.php';
		var values = 'process=1&' + $(this).parents().serialize();
		var parent = $(this).closest('div.tempContainer');
		var child = parent.children();
		var posts = $('div.posts');
		var editBox = $(this).closest('div#editBox');
		$(editBox).block({
			message: '<h1>Adding Post...</h1>',
			css: { border: '3px solid #a00' }
		});
		$.ajax({
			url: url,
			type: 'POST',
			data: values,
			success: function(data) {
				if ($(data).filter('div.error').length > 0) {
					parent.find('div#previewBox').html(data).show();
					$(editBox).unblock();
				} else {
					parent.find('div#previewBox').html('').hide();
					parent.find('div#previewBox').remove();
					$(editBox).unblock().remove();
					child.show().unwrap();
					child.find('a.addPostLink').removeClass('clicked');
					posts.prepend(data);
					$('div.box.message').wait(2000).fadeTo('normal', 0.0, function() {
						$(this).remove();
					});
				}
			}
		});
		return false;
	});

	// Click the "Update" button to update a post
	$('form#edit-form.edit-form button#update').live('click', function() {
		var url = 'edit.php';
		var parent = $(this).closest('div.tempContainer');
		var child = parent.children();
		var postID = child.find('input#postID').val();
		var posts = $('div.posts');
		var form =  'update=1&id='+postID+'&'+$(this).parents().serialize();
		var editBox = $(this).closest('div#editBox');
		$(editBox).block({
			message: '<h1>Updating...</h1>',
			css: { border: '3px solid #a00' }
		});
		$.ajax({
			url: url,
			type: 'POST',
			data: form,
			success: function(data) {
				if ($(data).filter('div.error').length > 0) {
					parent.find('div#previewBox').html(data).show();
					$(editBox).unblock();
				} else {
					parent.find('div#previewBox').html('').hide();
					parent.find('div#previewBox').remove();
					$(editBox).unblock().remove();
					child.unwrap();
					child.find('a.editPost').removeClass('clicked');
					child.html($(data).filter('div#post-'+postID).html()).show();
					$(data).filter('div.box.message').insertBefore(child);
					$('div.box.message').wait(2000).fadeTo('normal', 0.0, function() {
						$(this).remove();
					});
				}
			}
		});
		return false;
	});

	// Click the "Delete" button to delete a post
	$('form#edit-form.edit-form button#delete').live('click', function() {
		var editBox = $(this).closest('div#editBox');
		var question = editBox.filter('#question');
		$(editBox).block({
			message: $('#question'),
			css: {width: '400px', padding: '5px'}
		});
		return false;
	});

	// Confirm delete of the pust"
	$('div#editBox #yes').live('click', function() {
		var url = 'edit.php';
		var parent = $(this).closest('div.tempContainer');
		var child = parent.children();
		var postID = child.find('input#postID').val();
		var posts = $('div.posts');
		var form =  'delete=1&id='+postID;
		var editBox = $(this).closest('div#editBox');
		$(editBox).block({
			message: '<h1>Deleting...</h1>',
			css: { border: '3px solid #a00' }
		});
		$.ajax({
			url: url,
			type: 'POST',
			data: form,
			success: function(data) {
				if ($(data).filter('div.error').length > 0) {
					parent.find('div#previewBox').html(data).show();
					$(editBox).unblock();
				} else {
					$(editBox).unblock();
					$(data).insertBefore(parent);
					parent.remove();
					/*
					parent.find('div#previewBox').html('').hide();
					parent.find('div#previewBox').remove();
					parent.find('div#editBox').remove();
					child.show().unwrap();
					child.find('a.addPostLink').removeClass('clicked');
					posts.prepend(data);
					*/
					$('div.box.message').wait(2000).fadeTo('normal', 0.0, function() {
						$(this).remove();
					});
				}
			}
		});
		return false;
	});

	// Deny delete
	$('div#editBox #no').live('click', function() {
		var editBox = $(this).closest('div#editBox');
		$(editBox).unblock();
		return false;
	});

	var editingPost = $('form#edit-form');
	var postContent = document.getElementById('content');
	//$('div.inner-box').hide();

	// Surround text in textarea
	$('form#edit-form a.surround').live('click', function() {
		postContent = document.getElementById('content');
		var before = '';
		var after = '';
		switch($(this).find('img').attr('alt')) {
			case "bold":
				before = '[b]';
				after = '[/b]';
				break;
			case "italics":
				before = '[i]';
				after = '[/i]';
				break;
			case "underline":
				before = '[u]';
				after = '[/u]';
				break;
			case "strike":
				before = '[s]';
				after = '[/s]';
				break;
			case "glow":
				before = '[highlight=yellow]';
				after = '[/highlight]';
				break;
			case "shadow":
				before = '[shadow=red,left]';
				after = '[/shadow]';
				break;
			case "move":
				before = '[move]';
				after = '[/move]';
				break;
			case "pre":
				before = '[pre]';
				after = '[/pre]';
				break;
			case "left":
				before = '[left]';
				after = '[/left]';
				break;
			case "center":
				before = '[center]';
				after = '[/center]';
				break;
			case "right":
				before = '[right]';
				after = '[/right]';
				break;
			case "size":
				before = '[size=10]';
				after = '[/size]';
				break;
			case "face":
				before = '[font=Verdana]';
				after = '[/font]';
				break;
			case "flash":
				before = '[flash=200,200]';
				after = '[/flash]';
				break;
			case "img":
				before = '[img]';
				after = '[/img]';
				break;
			case "url":
				before = '[url]';
				after = '[/url]';
				break;
			case "email":
				before = '[email]';
				after = '[/email]';
				break;
			case "ftp":
				before = '[ftp]';
				after = '[/ftp]';
				break;
			case "table":
				before = '[table]';
				after = '[/table]';
				break;
			case "tr":
				before = '[tr]';
				after = '[/tr]';
				break;
			case "td":
				before = '[td]';
				after = '[/td]';
				break;
			case "sup":
				before = '[sup]';
				after = '[/sup]';
				break;
			case "sub":
				before = '[sub]';
				after = '[/sub]';
				break;
			case "tele":
				before = '[tt]';
				after = '[/tt]';
				break;
			case "code":
				before = '[code]';
				after = '[/code]';
				break;
			case "quote":
				before = '[quote]';
				after = '[/quote]';
				break;
			case "list":
				before = '[list]\n[li]';
				after = '[/li]\n[li][/li]\n[/list]';
				break;
		}

		surroundText(before, after, postContent);

		return false;
	});

	// Replace text in textarea
	$('form#edit-form a.replace').live('click', function() {
		postContent = document.getElementById('content');
		replaceText($(this).find('img').attr('alt'), postContent);
		if ($(this).find('img').attr('class') == 'emote') {
			$('form#edit-form div.emoticon div.inner-box').hide();
		}
		return false;
	});

	// Show emoticon box
	$('form#edit-form div.emoticon').live('mouseenter', function() {
		$('div.inner-box', this).show();
		if ($('div.inner-box img').attr('src') == '') {
			$('div.inner-box img').each(function(index) {
				$(this).attr('src', imgsrc[index]);
			});
		}
	});

	// Hide emoticon box
	$('form#edit-form div.emoticon').live('mouseleave',function() {
		$('div.inner-box', this).hide();
	});

	// Add font colors
	$('form#edit-form select#fontColor').live('change', function() {
		if ($(this).val() != '') {
			var before = '[color='+$(this).val()+']';
			var after = '[/color]';
			surroundText(before, after, postContent);
			$(this).find('option:first').attr('selected', 'selected').parent('select');
		}
		return false;
	});
});
