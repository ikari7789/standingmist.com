<table class="chart">
    <thead>
        <tr>
            <th colspan="3">Programming Portfolio</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <a href="http://vhaudio.com/newsletter/testissue.html"><img src="/img/portfolio/vhaudio-newsletter-thumb.jpg" alt="VHAudio Newsletter" /></a>
                <h3>VHAudio Newsletter</h3>
            </td>
            <td>
                <a href="http://www.v-cap.com/speaker-crossover-calculator.php"><img src="/img/portfolio/vcap-crossover-calc-thumb.jpg" alt="V-Cap Speaker Crossover Calculator" /></a>
                <h3>V-Cap Speaker Crossover Calculator</h3>
            </td>
            <td>
                <a href="http://www.v-cap.com/awg-calculator.php"><img src="/img/portfolio/vcap-awg-calc-thumb.jpg" alt="V-Cap AWG Calculator" /></a>
                <h3>V-Cap AWG Calculator</h3>
            </td>
        </tr>
        <tr>
            <td>
                <a href="http://old.standingmist.com/comprep/"><img src="/img/portfolio/comprep-thumb.jpg" alt="Home Computer Repair" /></a>
                <h3>Home Computer Repair</h3>
            </td>
            <td>
                <a href="http://old.standingmist.com/school/files/"><img src="/img/portfolio/files-thumb.jpg" alt="File Distributer" /></a>
                <h3>File Distributer</h3>
            </td>
            <td>
                <a href="http://old.standingmist.com/twband"><img src="/img/portfolio/twband-thumb.jpg" alt="Band Database" /></a>
                <h3>Band Database</h3>
            </td>
        </tr>
        <tr>
            <td>
                <a href="http://old.standingmist.com/electrophile"><img src="/img/portfolio/electrophile-thumb.jpg" alt="Personal Blog" /></a>
                <h3>Personal Blog</h3>
            </td>
            <td>
                <a href="http://old.standingmist.com/school/mcs325/"><img src="/img/portfolio/landing-page-thumb.jpg" alt="Main Landing Page" /></a>
                <h3>Main Landing Page</h3>
            </td>
            <td>
                <a href="http://old.standingmist.com/school/mcs325/a7/a7.html"><img src="/img/portfolio/tada-thumb.jpg" alt="Tada List Rip-off" /></a>
                <h3>Tada List Rip-off</h3>
            </td>
        </tr>
        <tr>
            <td>
                <a href="http://old.standingmist.com/jlc/"><img src="/img/portfolio/jlc-thumb.jpg" alt="Japanese Language Club" /></a>
                <h3>Japanese Language Club</h3>
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </tbody>
</table>
