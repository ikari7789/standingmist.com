<?php

function showEdit()
{
}

function showPosts()
{
    global $postMan;
    global $paginator;

    if ($_SESSION['loggedIn'] && (! isset($_GET['page']) || $_GET['page'] == 1)) {
        echo '<div class="box center" id="newPost">'."\n".
            '  <a href="" class="addPostLink">Add a Post</a>'."\n".
            '</div>'."\n";
    }
    echo '<div class="posts">'."\n";
    $postMan->getRecentPosts(POSTS_PER_PAGE);
    echo '</div>'."\n".
        '<div class="paginator">'."\n";
    $paginator->createPages();
    echo '</div>';
}

require_once 'lib/post.php';
require_once 'lib/paginator.php';
define('NUM_OF_PAGES', $paginator->numOfPages);

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['year'])) {
        // List posts in given year
        $postMan->getPostByDate($_GET);
    } elseif (isset($_GET['page'])) {
        if ($_GET['page'] == 'archive') {
            // Show archival of posts
            $postMan->getArchive();
        } elseif ($_GET['page'] == 'gallery') {
            // Show gallery
            include 'gallery.php';
            $gallery->showGallery();
        } elseif ($_GET['page'] == 'japanese') {
            include 'inc/construction.php';
        } elseif ($_GET['page'] == 'portfolio') {
            include 'inc/portfolio.php';
        } elseif ($_GET['page'] == 'register') {
            include 'register.php';
        } elseif ($_GET['page'] == 'contact') {
            include 'inc/contact.php';
        } elseif (is_numeric($_GET['page'])) {
            // Show posts on page XX
            showPosts();
        }
    } elseif (isset($_GET['tags'])) {
        // Show posts with given tag
        $postMan->getPostsByTags($_GET['tags']);
    } elseif (isset($_GET['post'])) {
        // Show post with given name
        $postMan->getPost($_GET['post']);
    } else {
        // Show post on page 1
        showPosts();
    }
}
