<div id="footer">
    <div class="border">
        <div class="body">
            <div class="copyright">Standing Mist Design and Code &copy; 2010<br />Andrew Miller</div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/js/jquery.wait.js"></script>
<script type="text/javascript" src="/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="/js/search.js"></script>
<script type="text/javascript" src="/js/posts.js"></script>
<script id="dsq-count-scr" src="//standingmist.disqus.com/count.js" async></script>
