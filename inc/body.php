<div id="body">
    <div class="border">
        <table id="container" cellpadding="0" cellspacing="0">
            <tr>
                <td id="leftcol">
                    <?php require_once 'leftcol.php'; ?>
                </td>
                <td class="spacer"></td>
                <td id="rightcol">
                    <?php require_once 'rightcol.php'; ?>
                </td>
            </tr>
        </table>
    </div>
</div>
