<div class="contact">
    <h2>Feel like getting in touch with me?</h2>
    <h3>Why not try using one of these mediums:</h3>
    <ul>
        <li><a href="mailto:milleraw@me.com">Email</a></li>
        <li><a href="andrew-miller-resume.pdf">Resume</a></li>
    </ul>
    <h2>Or perhaps you want to see my work?</h2>
    <h3>Those things are located here:</h3>
    <ul>
        <li><a href="http://www.flickr.com/people/25980881@N04/">Flickr</a></li>
        <li><a href="http://www.standingmist.com/gallery/">Personal Gallery</a></li>
        <li><a href="http://www.standingmist.com/programming/">Web design portfolio</a></li>
        <li><a href="http://www.youtube.com/ikari7789">YouTube</a></li>
    </ul>
</div>
