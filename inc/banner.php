<!--
<script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    pageLanguage: 'en',
    gaTrack: true,
    floatPosition: google.translate.TranslateElement.FloatPosition.BOTTOM_RIGHT
  });
}
</script><script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
-->
<div id="header">
    <div class="border">
        <div class="body">
            <div class="title">
                Standing in the Mist
            </div>
            <?php include_once 'login.php'; $loginSystem->displayLoginScreen(); ?>
        </div>
        <div class="navbar">
            <div class="links">
                <a href="/">Home</a> |
                <a href="/archive">Archive</a> |
                <a href="/gallery">Gallery</a> |
                <a href="/japanese">Japanese</a> |
                <a href="/portfolio">Portfolio</a> |
                <a href="/contact">Contact</a>
            </div>
            <div id="rssfeed">
                <a href="/rss"><img src="/img/social_networking_iconpack/rss_16.png" width="16" height="16" alt="Subscribe to RSS" /></a>
            </div>
        </div>
    </div>
</div>
