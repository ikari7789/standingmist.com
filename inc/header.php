<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php if (defined('TITLE')) {
    echo TITLE;
} ?></title>
    <!-- <base href="<?php echo BASE_URL; ?>" /> -->
    <link rel="icon" type="image/png" href="/img/favicon.png" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
    if (isset($metaInfo)) {
        echo $metaInfo."\n";
    }
?>
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
    <!--[if IE]>
        <link rel="stylesheet" type="text/css" href="/css/ie.css" />
    <![endif]-->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-6129302-2', 'standingmist.com');
        ga('send', 'pageview');

    </script>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
</head>
