<?php

class Post
{
    private $db;

    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    /**
     * Retrieve a post based on postID.
     */
    public function postInfo($postID)
    {
        $query = sprintf(
            'SELECT * FROM %sposts WHERE id = %d;',
            $this->db->escape($this->db->prefix),
            $postID
        );

        $result = $this->db->query($query) or die($this->db->error());

        return $result;
    }

    /**
     * Retrieve a post's tags based on $postID.
     */
    public function getTags($postID)
    {
        $query = sprintf(
            'SELECT tag FROM %stags WHERE postID = %d ORDER BY tag DESC',
            $this->db->escape($this->db->prefix),
            $postID
        );

        $result = $this->db->query($query);

        // If query succeeded
        if (! is_bool($result)) {
            // If tags aren't found
            if ($this->db->numRows($result) == 0) {
                echo '<b>none</b>'; // Report none
            } else { // Show all tags
                while ($tag = $this->db->fetchArray($result, MYSQLI_ASSOC)) {
                    echo '<a href="/tags/'.$tag['tag'].'" class="tag">'.$tag['tag'].'</a> ';
                }
            }
        } else { // Report none if query failed
           echo '<b>none</b>';
        }
    }

    public function closeHTML($html)
    {
        $arr_single_tags = ['meta','img','br','link','area', 'hr', 'input'];

        preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
        $openedtags = $result[1];

        preg_match_all('#</([a-z]+)>#iU', $html, $result);
        $closedtags = $result[1];

        $len_opened = count($openedtags);
        if (count($closedtags) == $len_opened) {
            return $html;
        }

        $openedtags = array_reverse($openedtags);

        for ($i = 0; $i < $len_opened; $i++) {
            if (! in_array($openedtags[$i], $arr_single_tags)) {
                if (! in_array($openedtags[$i], $closedtags)) {
                    if (count($openedtags) < $i + 1 && $next_tag = $openedtags[$i + 1]) {
                        $html = preg_replace('#</'.$next_tag.'#iU', '</'.$openedtags[$i].'></'.$next_tag, $html);
                    } else {
                        $html .= '</'.$openedtags[$i].'>';
                    }
                }
            }
        }

        return $html;
    }

    /**
     * Output a post to the web browser given a posts
     * information and whether or not to show the
     * entire post or not.
     */
    public function displayPost($post, $truncate = true, $preview = false)
    {
        // $post variables:
        // $post['id'] = Post ID
        // $post['uniqueTitle'] = Title unique to one post
        // $post['title'] = Public title
        // $post['author'] = Post author
        // $post['content'] = Post content
        // $post['lastUpdate'] = Post last updated
        // $post['date'] = Original posting date
        // $post['tags'] = Tags relating to post
        if (! $preview) {
            ?>
<div class="box" id="post-<?php echo $post['id'];
            ?>" >
<?php

        }
        ?>
<div class="post-header">
    <div class="post-title" id="<?php echo $post['uniqueTitle'];
        ?>">
        <a href="/post/<?php echo $post['uniqueTitle']; // echo the link to the post's own page?>"><?php echo $post['title'];
        ?></a>
<?php
        if (isset($_SESSION)) {
            // If the user logged in is the one who wrote the post
            // allow them to edit it
            if ($_SESSION['loggedIn'] && $_SESSION['user']['username'] == $post['author']) {
                echo '  <a href="/edit/'.$post['uniqueTitle'].'" class="editPost">Edit post?</a>';
            }
        }
        ?>
</div>
<div class="author">
    <?php echo $post['author'];
        ?>
</div>
<?php
        if ($preview) {
            ?>
<div class="tags">
    Tags: <?php echo $post['tags'];
            ?>
</div>
<?php

        } else {
            ?>
<div class="tags">
    Tags: <?php $this->getTags($post['id']); // get tags ?>
</div>
<?php

        }
        ?>
</div>
<div class="post-body">
    <div class="post-content">
        <script type="text/javascript">
            submit_url = '<?php echo ($_SERVER['HTTPS']) ? 'https://'.$_SERVER['HTTP_HOST'].'/' : 'http://'.$_SERVER['HTTP_HOST'].'/';
        ?>post/<?php echo $post['uniqueTitle'];
        ?>';
        </script>
<?php
        // bbcode_format -
        //   Switches out the allowed tags with HTML code to allow styling on posts
        $content = bbcode_format($post['content']);
        if ($truncate) {
            // Set variables for the max number of lines or max number of characters
            $MAX_LINES = 13;
            $MAX_CHAR = 550;

            // Count the number of lines based on <br /> found
            $lines = count(explode('<br />', $content));

            // If the post exceeds the limits
            if (strlen($content) > $MAX_CHAR || $lines > $MAX_LINES) {
                if (strlen($content) > $MAX_CHAR) {
                    // If the post exceeds the chars too
                    // Shrink that bitch down
                    $preview = substr($content, 0, $MAX_CHAR);

                    // Check to make sure that I didn't cut off any tags
                    $openingTag = strrpos($preview, '<');
                    $closingTag = strrpos($preview, '>');
                    if ((is_numeric($openingTag) && is_numeric($closingTag) && $openingTag > $closingTag) || (is_numeric($openingTag) && $closingTag === false)) {
                        $preview = substr($content, 0, strpos($content, '>', $openingTag) + 1);
                    }

                    $preview = $this->closeHTML($preview);

                    $additional = substr($content, $MAX_CHAR);
                    //echo 'openingTag = '.$openingTag.
                    //    '<br />closingTag = '.$closingTag.
                    //    '<br />strpos($content, ">", $openingTag) = '.(strpos($content, ">", $openingTag) + 1).'<br />';
                    echo $preview;
                } elseif ($lines > $MAX_LINES) {
                    // If there are more lines than allowed,
                    // Create a preview limited at the max
                    $preview = explode('<br />', $content, $MAX_LINES);
                    echo $preview[0];
                    for ($line = 1; $line < $MAX_LINES - 1; $line++) {
                        echo '<br />'.$preview[$line];
                    }
                    $additional = '<br />'.$preview[$MAX_LINES - 1];
                }

                echo '<br /><a class="full-post" href="/post/'.$post['uniqueTitle'].'">View More</a>';
                // Code to include entire post in main
                // I've since decided against showing the entire post
                // in the main window since this will shrink down on data sent
                // echo '<span class="addkey">...  <a href="javascript:void(0);" onclick="showAdditional('.$post['id'].');">View more</a></span>';
                // echo '<span class="additional">'.$additional.'</span>';
            } else {
                // If the post isn't truncated, echo the whole thing
                echo $content;
            }
        } else {
            echo $content;
        }

        if ($post['lastUpdated'] != '0000-00-00 00:00:00') {
            echo '<div class="lastUpdate">'."\n".
                "\t".'<span class="updated">Last updated: <i>'.$post['lastUpdated'].'</i></span>'."\n".
                '</div>'."\n";
        }

        if (! $truncate) {
            ?>
<div class="social">
    <!-- AddThis Toolbox - Big Icons -->
    <script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
    <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=ikari7789"></script>
    <div class="addthis_toolbox">
        <div class="custom_images">
            <a class="addthis_button_facebook"><img src="/img/social_networking_iconpack/facebook_32.png" width="32" height="32" alt="Share to Facebook" /></a>
            <a class="addthis_button_twitter"><img src="/img/social_networking_iconpack/twitter_32.png" width="32" height="32" alt="Share to Twitter" /></a>
            <a class="addthis_button_email"><img src="/img/social_networking_iconpack/email_32.png" width="32" height="32" alt="Email This" /></a>
            <a class="addthis_button_more"><img src="/img/social_networking_iconpack/addthis_32.png" width="32" height="32" alt="More..." /></a>
        </div>
    </div>
</div>
<?php

        }
        ?>
    </div>
</div>
<div class="post-footer">
<?php
        if (! isset($_GET['post'])) {
            echo '<a href="/post/'.$post['uniqueTitle'].'#disqus_thread" class="comment">Comments</a>';
        }
        ?>
        <h3 class="date"><?php echo $post['date'];
        ?></h3>
    </div>
</div>
<?php
        if (isset($_GET['post'])) {
            ?>
<div class="box" id="comments">
    <div id="disqus_thread"></div>
    <script>
        var disqus_config = function () {
            this.page.url = '<?php echo BASE_URL."/post/{$post['uniqueTitle']}"; ?>';
            this.page.identifier = '<?php echo $post['uniqueTitle']; ?>';
        };
        (function() {  // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');

            s.src = '//standingmist.disqus.com/embed.js';

            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
<?php
             if (! $preview) {
?>
</div>
<?php
             }
        }
    }

    /**
     * Create a unique title for use in
     * URI redirects directly to the artical
     * in pretty URIs
     * the method used might change in the
     * future, so I've externalized it.
     *
     * Forgot to check for duplicate titles - since main titles could be the same
     */
    public function createUniqueTitle($title)
    {
        $title = strtolower($title);
        $title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');
        $count = '';
        $query = sprintf(
            'SELECT COUNT(title) FROM %sposts WHERE title = "%s";',
            $this->db->escape($this->db->prefix),
            $this->db->escape(trim($title))
        );
        $result = $this->db->query($query);
        $numPosts = $this->db->fetchArray($result, MYSQLI_ASSOC);
        if ($numPosts['COUNT(title)'] >= 1) {
            $count = $numPosts['COUNT(title)'] + 1;
            $count = '-'.$count;
        }

        $replace['pre'] = [' ', '#', '^', '%', '?', '/', '!', '\''];
        $replace['post'] = ['-', '-num-', '', '', '', '', '', '-'];

        $uniqueTitle = str_replace($replace['pre'], $replace['post'], $title).$count;

        return $uniqueTitle;
    }

    /**
     * Split the tags based on the delimiter
     * The delimiter might change in the future,
     * so I externalized the split.
     */
    public function splitTags($tags)
    {
        $newTags = preg_split('/( |　)/', $tags);

        return array_unique($newTags);
    }

    /**
     * Add a post to the database.
     */
    public function addPost($post)
    {
        // Initialize Variables
        $uniqueTitle = $this->createUniqueTitle($post['title']);
        $tags = $this->splitTags($post['tags']);

        // Add Post into the database
        $query = sprintf(
            'INSERT INTO %sposts (uniqueTitle, title, author, date, content) VALUES ("%s", "%s", "%s", %s, "%s");',
            $this->db->escape($this->db->prefix),
            $this->db->escape(trim($uniqueTitle)),
            $this->db->escape(trim($post['title'])),
            $this->db->escape(trim($post['author'])),
            'NOW()',
            $this->db->escape(trim($post['content']))
        );
        $this->db->query($query) or die($this->db->error());

        // Query the newly added post
        // Need postID for adding tags
        // Need post data for displaying new post
        $query = sprintf(
            'SELECT * FROM %sposts WHERE uniqueTitle = "%s";',
            $this->db->escape($this->db->prefix),
            $this->db->escape(trim($uniqueTitle))
        );
        $result = $this->db->query($query);

        $post = $this->db->fetchArray($result, MYSQLI_ASSOC);

        // Add tags for that post
        foreach ($tags as $tag) {
            $query = sprintf(
                'INSERT INTO %stags (postID, tag) VALUES (%d, "%s");',
                $this->db->escape($this->db->prefix),
                $post['id'],
                $this->db->escape(trim($tag))
            );
            $this->db->query($query) or die($this->db->error());
        }

        // Return message
        $message = '<div class="box message">New post added!</div>';
        echo $message;
        $this->displayPost($post);
    }

    public function updatePost($postValues)
    {
        $oldPost = $this->db->fetchArray($this->postInfo($postValues['id']), MYSQLI_ASSOC);

        if ($oldPost['title'] == $postValues['title']) {
            $uniqueTitle = $oldPost['uniqueTitle'];
        } else {
            $uniqueTitle = $this->createUniqueTitle($postValues['title'], true);
        }
        $query = sprintf(
            'UPDATE %sposts SET uniqueTitle = "%s", title="%s", lastUpdated=%s, content="%s" WHERE id=%d;',
            $this->db->escape($this->db->prefix),
            $this->db->escape($uniqueTitle),
            $this->db->escape($postValues['title']),
            'NOW()',
            $this->db->escape($postValues['content']),
            $this->db->escape($postValues['id'])
        );
        $this->db->query($query) or die(mysqli_error());

        // Example queries for updating tags:
        //
        // INSERT IGNORE INTO blog_tags (postID, tag)
        // VALUES (10, "test1"), (10, "test2"), (10, "test3"), (10, "test4");
        //
        // DELETE FROM blog_tags
        // WHERE postID = 10 AND tag NOT IN ("test1", "test2", "test3", "test4");

        // Split up the tag string and then insert the new tags
        $tags = $this->splitTags($postValues['tags']);
        $tagString = $this->tagsToString($tags, $postValues['id']);

        $query = sprintf(
            'INSERT IGNORE INTO %stags (postID, tag) VALUES %s;',
            $this->db->escape($this->db->prefix),
            $tagString
        );
        $this->db->query($query) or die($this->db->error());

        // Delete any tags that might were there previously to updating
        $tagString = $this->tagsToString($tags);
        $query = sprintf(
            'DELETE FROM %stags WHERE postID = %d AND tag NOT IN (%s);',
            $this->db->escape($this->db->prefix),
            $postValues['id'],
            $tagString
        );
        $this->db->query($query) or die($this->db->error());

        // Retrieve the updated post to send to get to displayed
        $query = sprintf(
            'SELECT * FROM %sposts WHERE id = %d',
            $this->db->escape($this->db->prefix),
            $postValues['id']
        );
        $result = $this->db->query($query);

        $post = $this->db->fetchArray($result);

        // Return message
        $message = '<div class="box message">Post updated!</div>';
        echo $message;
        $this->displayPost($post);
    }

    public function deletePost($postID)
    {
        $post = $this->db->fetchArray($this->postInfo($postID), MYSQLI_ASSOC);
        if ($_SESSION['loggedIn'] && $post['author'] == $_SESSION['user']['username']) {
            // Delete post
            $query = sprintf(
                'DELETE FROM %sposts WHERE id = %d;',
                $this->db->escape($this->db->prefix),
                $postID
            );
            $this->db->query($query) or die($this->db->error());

            // Delete comments
            $query = sprintf(
                'DELETE FROM %s%scomments WHERE id= %d;',
                $this->db->escape($this->db->prefix),
                $postID
            );
            $this->db->query($query) or die($this->db->error());

            // Delete tags
            $query = sprintf(
                'DELETE FROM %s%stags WHERE postID = %d;',
                $this->db->escape($this->db->prefix),
                $postID
            );
            $result = $this->db->query($query) or die($this->db->error());
            echo '<div class="box message">Post successfully deleted.</div>';
        } else {
            print_r($post);
            echo '<div class="error">Must be logged in as '.$post['author'].' in order to delete this post.</div>';
        }
    }

    /**
     * Display $numOfPosts posts in order of newest->oldest.
     */
    public function getRecentPosts($numOfPosts)
    {
        // Initialize Variables
        $limit = '';

        // Determine which page of results to show if page is set
        if (isset($_GET['page'])) {
            $page = $_GET['page'];

            // If the page requested exceeds the total number of pages
            // Redirect to the last page
            if ($page > NUM_OF_PAGES) {
                $page = NUM_OF_PAGES;
            }

            // If the page request is less than the lowest page...
            // Redirect to the first page
            if ($page < 1) {
                $page = 1;
            }

            // Create a limiter based on how many posts should be shown
            $limit = ' LIMIT '.(($page - 1) * $numOfPosts).', '.$numOfPosts;
        } else {
            $limit = ' LIMIT '.$numOfPosts;
        }

        // Query posts
        $query = sprintf(
            'SELECT * FROM %sposts WHERE date < NOW() ORDER BY date DESC'.$limit.';',
            $this->db->escape($this->db->prefix)
        );
        $result = $this->db->query($query);

        // If no posts found
        if ($this->db->numRows($result) == 0) {
            // Report to user that nothing is yet added to the database
            $message = '<span class="center">Sorry, there are not any entries added to the database yet.</span>';
            echo '<div class="innerbox">'."\n".
                '  <p>'.$message.'</p>'."\n".
                '</div>'."\n";
        } else {
            // Work with result data
            while ($post = $this->db->fetchArray($result, MYSQLI_ASSOC)) {
                if (is_array($post)) {
                    $this->displayPost($post, true);
                }
            }
        }
    }

    /**
     * Show an archival listing of posts based on the $date passes.
     */
    public function getPostByDate($date)
    {
        // If the year, month, and day are set
        if (isset($_GET['year']) && isset($_GET['month']) && isset($_GET['day'])) {
            // Get the posts on that exact day
            $query = sprintf(
                'SELECT * FROM %sposts WHERE YEAR(date) = %d AND MONTH(date) = %d AND DAY(date) = %d;',
                $this->db->escape($this->db->prefix),
                $_GET['year'],
                $_GET['month'],
                $_GET['day']
            );
        } elseif (isset($_GET['year']) && isset($_GET['month']) && ! isset($_GET['day'])) {
            // If the year and month are set
            // Get all posts in that month
            $query = sprintf(
                'SELECT * FROM %sposts WHERE YEAR(date) = %d AND MONTH(date) = %d;',
                $this->db->escape($this->db->prefix),
                $_GET['year'],
                $_GET['month']
            );
        } elseif (isset($_GET['year']) && ! isset($_GET['month']) && ! isset($_GET['day'])) {
            // The year is set
            // Get all posts in that year
            $query = sprintf(
                'SELECT * FROM %sposts WHERE YEAR(date) = %d;',
                $this->db->escape($this->db->prefix),
                $_GET['year']
            );
        }
        $result = $this->db->query($query);

        if ($this->db->numRows($result) > 0) {
            // Display links to corresponding posts
            while ($post = $this->db->fetchArray($result, MYSQLI_ASSOC)) {
                echo '<div class="archive-item"><span class="title"><a href="/post/'.$post['uniqueTitle'].'">'.$post['title'].'</a></span><span class="post-date">'.$post['date'].'</span></div>';
            }
        } else {
            // If no results to show
            echo '<div class="error">Sorry, no posts where found.</div>';
        }
    }

    /**
     * Create a tag representing the tags for a post
     * $tags is an array of tags.
     */
    public function tagsToString($tags, $postID = null)
    {
        // Create a string to display for tags in the post
        $tagString = '';
        foreach ($tags as $tag) {
            if ($postID == null) {
                $tagString .= '"'.mysqli_real_escape_string($tag).'", ';
            } else {
                $tagString .= '('.$postID.', "'.mysqli_real_escape_string($tag).'"), ';
            }
        }

        // Remove the last ', ' in the string
        $tagString = substr($tagString, 0, strlen($tagString) - 2);

        return $tagString;
    }

    /**
     * EXPERIMENTAL
     * Display archival posts by tags
     * For the time being, I believe it's working! :).
     */
    public function getPostsByTags($tagline)
    {
        // Initialize variables
        $tags = $this->splitTags($tagline);
        $tagString = $this->tagsToString($tags);

        // Select all data from a post where the tag is contained.
        $query = sprintf(
            'SELECT * FROM %sposts p, %stags t WHERE p.id = t.postID AND t.tag IN (%s) GROUP BY t.postID HAVING COUNT(t.postID) = %d ORDER BY p.date DESC ;',
            $this->db->escape($this->db->prefix),
            $this->db->escape($this->db->prefix),
            $tagString,
            count($tags)
        );
        $result = $this->db->query($query);

        // If there are posts found
        if (! is_bool($result) && $this->db->numRows($result) > 0) {
            // Display archivaly
            while ($post = $this->db->fetchArray($result, MYSQLI_ASSOC)) {
                echo '<div class="archive-item"><span class="title"><a href="/post/'.$post['uniqueTitle'].'">'.$post['title'].'</a></span><span class="post-date">'.$post['date'].'</span></div>';
            }
        } else {
            // Show that there are no posts with that tag
            echo '<div class="error">Sorry, no posts where found.</div>';
        }
    }

    /**
     * Show all posts *ever*
     * in an archival layout.
     */
    public function getArchive()
    {
        // Select all posts
        $query = sprintf(
            'SELECT * FROM %sposts',
            $this->db->escape($this->db->prefix)
        );
        $result = $this->db->query($query);

        // If posts found,
        if ($this->db->numRows($result) > 0) {
            // Display archivaly
            while ($post = $this->db->fetchArray($result, MYSQLI_ASSOC)) {
                echo '<div class="archive-item"><span class="title"><a href="/post/'.$post['uniqueTitle'].'">'.$post['title'].'</a></span><span class="post-date">'.$post['date'].'</span></div>';
            }
        } else {
            // Explain error to user
            echo '<div class="error">Sorry, no posts where found.</div>';
        }
    }

    /**
     * Used to show a whole post on it's own page.
     */
    public function getPost($title)
    {
        // Select the post defined by its $title
        $query = sprintf(
            'SELECT * FROM %sposts WHERE uniqueTitle = "%s";',
            $this->db->escape($this->db->prefix),
            $this->db->escape($title)
        );
        $result = $this->db->query($query);

        // If the post is found
        if ($this->db->numRows($result) == 1) {
            // Write it out to the web browser
            $post = $this->db->fetchArray($result, MYSQLI_ASSOC);
            $this->displayPost($post, false);
        } else {
            // Inform the user of the error
            echo '<div class="message">Sorry, but there is no post named "'.$_GET['post'].'"</div>';
        }
    }

    /**
     * Create a barebones version of a post for AJAX usage.
     */
    public function toHtml($post)
    {
        // Query the post given by $title
        $query = sprintf(
            'SELECT * FROM %sposts WHERE uniqueTitle = "%s";',
            $this->db->escape($this->db->prefix),
            $this->db->escape($post)
        );
        $result = $this->db->query($query);

        // If the post is found
        if (! is_bool($result)) {
            if ($this->db->numRows($result) == 1) {
                // Create a bare-bones post
                echo '<div id="post">'."\n";
                $post = $this->db->fetchArray($result, MYSQLI_ASSOC);
                foreach ($post as $key => $value) {
                    echo '  <div id="'.$key.'">'.$value.'</div>'."\n";
                }
                echo '</div>';
            }
        }
    }

    public function search($title)
    {
        $query = sprintf(
            'SELECT DISTINCT * FROM %sposts WHERE title LIKE "%%%s%%";',
            $this->db->escape($this->db->prefix),
            $this->db->escape($title)
        );
        $result = $this->db->query($query);

        if ($this->db->numRows($result) > 0) {
            // Display archivaly
            echo '<div class="search-results">'."\n";
            while ($post = $this->db->fetchArray($result, MYSQLI_ASSOC)) {
                echo '  <div class="archive-item"><span class="title"><a href="/post/'.$post['uniqueTitle'].'">'.$post['title'].'</a></span><span class="post-date">'.$post['date'].'</span></div>';
            }
            echo '</div>';
        } else {
            // Explain error to user
            echo '<div class="error">Sorry, no posts where found.</div>';
        }
    }
}

$postMan = new Post;

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['edit'])) {
        include_once 'constants.php';
        include_once 'mysqli.php';
        echo $postMan->toHtml($_GET['post']);
    }
}
