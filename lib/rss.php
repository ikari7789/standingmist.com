<?php

require_once 'constants.php';
require_once 'mysqli.php';

class RSS
{
    private $db;

    public function __construct()
    {
        global $db;
        $this->db = $db;
        $this->draw();
    }

    public function draw()
    {
        echo '<?xml version="1.0" encoding="utf-8"?>'."\n".
            '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">'."\n".
            '  <channel>'."\n".
            '    <title>Standing in the Mist RSS Feed</title>'."\n".
            '    <atom:link href="http://'.$_SERVER['SERVER_NAME'].'/rss" rel="self" type="application/rss+xml" />'."\n".
            '    <link>http://'.$_SERVER['SERVER_NAME'].'</link>'."\n".
            '    <description>Post archival of Standing in the Mist Blog</description>'."\n".
            '    <language>en</language>'."\n";
        $this->printPosts();
        echo '  </channel>'."\n".
            '</rss>';
    }

    public function printPosts()
    {
        $query = sprintf(
            'SELECT * FROM %sposts ORDER BY date DESC;',
            $this->db->escape($this->db->prefix)
        );
        $result = $this->db->query($query);

        while ($post = $this->db->fetchArray($result, MYSQLI_ASSOC)) {
            if (strlen($post['content']) > 300) {
                $description = substr($post['content'], 0, 300).'...';
            } else {
                $description = $post['content'];
            }
            echo '    <item>'."\n".
                '     <title>'.$post['title'].'</title>'."\n".
                '     <link>http://'.$_SERVER['SERVER_NAME'].'/post/'.$post['uniqueTitle'].'</link>'."\n".
                '     <guid>http://'.$_SERVER['SERVER_NAME'].'/post/'.$post['uniqueTitle'].'</guid>'."\n".
                '     <description>'.$description.'</description>'."\n".
                '     <pubDate>'.date('r', strtotime($post['date'])).'</pubDate>'."\n".
                '    </item>'."\n";
        }
    }
}

header('Content-type: application/rss+xml');
$rss = new RSS();
