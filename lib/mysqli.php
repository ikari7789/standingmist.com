<?php

/**
 * The following class handles all MySQL database interactions.
 */
class MySQL
{
    public $host;
    public $user;
    public $pass;
    public $name;
    public $prefix;
    public $result;
    private $conn;

    public function __construct($host, $user, $pass, $name, $prefix)
    {
        $this->host = $host;
        $this->user = $user;
        $this->pass = $pass;
        $this->name = $name;
        $this->prefix = $prefix;
        $this->connect();
    }

    /**
     * Connect to the database.
     */
    public function connect()
    {
        $this->conn = mysqli_connect($this->host, $this->user, $this->pass) or die('Error connecting to mysqli');
        mysqli_select_db($this->conn, $this->name);
        mysqli_query($this->conn, "SET NAMES 'utf8'");
    }

    public function query($query)
    {
        return mysqli_query($this->conn, $query);
    }

    public function error()
    {
        return mysqli_error($this->conn);
    }

    public function escape($escapestr)
    {
        return mysqli_real_escape_string($this->conn, $escapestr);
    }

    public function fetchArray($result, $resulttype = MYSQLI_BOTH)
    {
        return mysqli_fetch_array($result, $resulttype);
    }

    public function numRows($result)
    {
        return mysqli_num_rows($result);
    }

    /**
     * Disconnect from the database.
     */
    public function __deconstruct()
    {
        mysqli_close($this->conn);
    }
}

$db = new MySQL(DB_HOST, DB_USER, DB_PASS, DB_NAME, DB_PREFIX);
