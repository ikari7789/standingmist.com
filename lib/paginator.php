<?php

class Paginator
{
    public $perPage = POSTS_PER_PAGE;
    public $numOfPages = 0;
    private $db;

    public function __construct()
    {
        global $db;
        $this->db = $db;
        $this->getNumPages();
    }

    public function getNumPages()
    {
        $total = 0;

        // Retrieve the number of bands in the database;
        $query = sprintf(
            'SELECT COUNT(id) AS numOfPosts FROM %sposts;',
            $this->db->prefix
        );
        $result = $this->db->query($query) or die($this->db->error());
        $number = $this->db->fetchArray($result);
        $total = $number['numOfPosts'];

        // Determins the # of pages, if the number is a decimal value,
        // it rounds up to the nearest whole number.
        $this->numOfPages = ceil($total / $this->perPage);
    }

    /**
     * This function decides how many pages to make,
     * determined by the number of results per page,
     * and the total number of items there are in total.
     */
    public function createPages()
    {
        // Determine special stylying for current page
        $currentPage = 1;
        if (isset($_GET['page'])) {
            $currentPage = $_GET['page'];
        }

        // Set first and last pages;
        $firstPage = 1;
        $lastPage = 9;

        // Display only 10 pages at a time
        if ($this->numOfPages > 9) {
            $firstPage = $currentPage - 4;
            $lastPage = $currentPage + 4;

            // Determine if there's even enough pages to show.
            if ($firstPage < 1) {
                $firstPage = 1;
                $lastPage = 9;
            }

            if ($lastPage > $this->numOfPages) {
                $lastPage = $this->numOfPages;
                $firstPage = $this->numOfPages - 8;
            }
        } elseif ($this->numOfPages < 9) {
            $lastPage = $this->numOfPages;
        }

        $previousPage = $currentPage - 1;
        $nextPage = $currentPage + 1;

        if ($previousPage < 1) {
            $previousPage = 1;
        }
        if ($nextPage > $this->numOfPages) {
            $nextPage = $this->numOfPages;
        }
        if ($currentPage > $this->numOfPages) {
            $currentPage = $this->numOfPages;
        }

        if ($this->numOfPages > 1) {
            echo '<div class="box pagination">'."\n".
                 '  <ul class="pagination">'."\n".
                 '    <li class="page" id="first-page"><a href="page/1">First</a></li>'."\n".
                 '    <li class="page" id="previous-page"><a href="page/'.$previousPage.'">&lt;&lt;</a></li>'."\n";
            for ($page = $firstPage; $page <= $lastPage; $page++) {
                if ($page == $currentPage) {
                    echo '    <li class="page" id="current-page"><a href="page/'.$page.'">'.$page.'</a></li>'."\n";
                } else {
                    echo '    <li class="page"><a href="page/'.$page.'">'.$page.'</a></li>'."\n";
                }
            }
            echo '    <li class="page" id="next-page"><a href="page/'.$nextPage.'">&gt;&gt;</a></li>'."\n".
                 '    <li class="page" id="last-page"><a href="page/'.$this->numOfPages.'">Last</a></li>'."\n".
                 '  </ul>'."\n".
                 '</div>';
        }
    }
}

$paginator = new Paginator();
