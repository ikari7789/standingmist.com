<?php

class Url
{
    public $url;

    public function __construct()
    {
        $this->url = $this->grabUrl();
    }

    /**
     * This function adds a GET value
     * to a URL without a value.
     */
    public function addToURLNoVal($url, $name)
    {
        $seperator = '?';
        if (strpos($url, $seperator) !== false) {
            $seperator = '&';
        }

        return $url.$seperator.$name.'=';
    }

    /**
     * This function adds or updates a GET value
     * appended on to the end of the URL.
     */
    public function addToURL($url, $name, $value)
    {
        $newurl = $url;

        // Check if new addition already exists in the url
        if (preg_match('/'.$name.'=/', $newurl)) {
            if (preg_match('/'.$name.'='.$value.'(&|#|$)/', $newurl)) {
                return $newurl;
            } else {
                if (strpos($newurl, '=') == strrpos($newurl, '=')) {
                    return preg_replace('/'.$name.'=[A-Za-z0-9]{1,100}(&|#|\b)/', $name.'='.$value, $newurl);
                } else {
                    return preg_replace('/'.$name.'=[A-Za-z0-9]{1,100}/', $name.'='.$value, $newurl);
                }
            }
        }

        // Pick the correct separator to use
        $separator = '?';
        if (strpos($newurl, '?') !== false) {
            $separator = '&';
        }

        // Find the location for the new parameter
        $insertPosition = strlen($newurl);
        if (strpos($newurl, '#') !== false) {
            $insertPosition = strpos($newurl, '#');
        }

        // Build the new url
        $newurl = substr_replace($newurl, "$separator$name=$value", $insertPosition, 0);

        // prints:http://www.vnoel.com?cmd=list&option=new
        return $newurl;
    }

    /**
     * This function gets the url currently in
     * the address bar.
     */
    public function grabUrl()
    {
        // Get the information from the address bar
        $url = $_SERVER['PHP_SELF'];
        $getVals = $_GET;

        foreach ($getVals as $variable => $value) {
            $url = addToUrl($url, $variable, $value);
        }

        return $url;
    }
}

$url = new Url();
