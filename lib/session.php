<?php

include_once 'constants.php';
include_once 'mysqli.php';
include_once 'functions.php';

if (! isset($_SESSION['loggedIn'])) {
    $_SESSION['loggedIn'] = false;
}
