<?php

/**
 * This function checks if an image for a particular
 * band exists. If so, the location of it's image is given.
 * If not, then the default image location is returned.
 */
function imageLocation($filePath, $name, $ext, $alt)
{
    $filename = $filePath.$name.'.'.$ext;
    //$filename = dirname(__FILE__).'\\img\\bands\\'.$imgID.'.png';
    //$filename = str_replace('\\', '/', $filename);
    if (file_exists($filename)) {
        return $filename;
    } else {
        return $alt;
    }
}

function bbcode_format($str)
{
    // Convert all special HTML characters into entities to display literally
    $str = htmlspecialchars($str, ENT_QUOTES, 'UTF-8');

    // The array of regex patterns to look for
    $format_search = [
        '#\[b\]\n?(.*?)\[/b\]#is', // Bold ([b]text[/b]
        '#\[i\]\n?(.*?)\[/i\]#is', // Italics ([i]text[/i]
        '#\[u\]\n?(.*?)\[/u\]#is', // Underline ([u]text[/u])
        '#\[s\]\n?(.*?)\[/s\]#is', // Strikethrough ([s]text[/s])
        '#\[highlight=(\#?[A-F0-9]{3}|\#?[A-F0-9]{6}|[a-z]+)\]\n?(.*?)\[/highlight\]#is', // Highlight ([highlight=red, 2, 300]text[/highlight]
        '#\[shadow=([0-9]+), ?([0-9]+), ?([0-9]+), ?(\#?[A-F0-9]{3}|\#?[A-F0-9]{6}|[a-z]+)\]\n?(.*?)\[/shadow\]#is',// Shadow goes here...
        // might need a special area for shadow due to it needing some process
        '#\[move\]\n?(.*?)\[/move\]#is', // Marquee ([move]text[/move])
        '#\[pre\]\n?(.*?)\[/pre\]\n#is', // Preformatted Text ([pre]text[/pre])
        '#\[left\]\n?(.*?)\[/left\]#is', // Left Aligned ([left]text[/left])
        '#\[center\]\n?(.*?)\[/center\]#is', // Centered ([center]text[/center])
        '#\[right\]\n?(.*?)\[/right\]#is', // Right Aligned ([right]text[/right])
        '#\[hr]\n?#is', // Horizontal Rule ([hr])
        '#\[size=([0-9]+)\]\n?(.*?)\[/size\]#is', // Font size 1-20px [size=20]text[/size])
        '#\[font=(.*?)\]\n?(.*?)\[/font\]#is',// Font Face goes here...
        // need a special algorithm to check fonts
        '#\[color=(\#?[A-F0-9]{3}|\#?[A-F0-9]{6}|[a-z]+)\]\n?(.*?)\[/color\]#is', // Font color ([color=#00F]text[/color])
        '#\[flash=([0-9]+), ?([0-9]+)\]\n?(.*?)\[/flash\]#is', // Flash Animation ([flash=width,height]link to flash animation[/flash]
        '#\[img alt=([A-Za-z0-9\W\s]+)\]\n?(.*?)\[/img\]#is', // Image with descriptive text ([img=Alt Text]link to image[/img])
        '#\[img\]\n?(.*?)\[/img\]#is', // Image ([img]link to image[/img])
        '#\[img size=([0-9]+), ?([0-9]+)\]\n?(.*?)\[/img\]#is', // Add option for custom sized image later on. Shouldn't be too tough, just need a few more Regex's
        '#\[img size=([0-9]+), ?([0-9]+) alt=([A-Za-z0-9\W\s]+)\]\n?(.*?)\[/img\]#is',
        '#\[img float=(left|right|none) size=([0-9]+), ?([0-9]+) alt=([A-Za-z0-9\W\s]+)\]\n?(.*?)\[/img\]#is',
        '#\[url=(https?://.*?)\]\n?(.*?)\[/url\]#is', // Hyperlink with descriptive text ([url=http://url]text[/url])
        '#\[url\]\n?(https?://.*?)\[/url\]#is', // Hyperlink ([url]http://url[/url])
        '#\[url=(.*?)\]\n?(.*?)\[/url\]#is', // Hyperlink with descriptive text ([url=http://url]text[/url])
        '#\[url\]\n?(.*?)\[/url\]#is', // Hyperlink ([url]http://url[/url])
        '#\[email\]\n?(.*?)\[/email\]#is', // Email ([email]email@domain.com[/email])
        '#\[ftp=(ftp://.*?)\]\n?(.*?)\[/ftp\]#is', // FTP link with descriptive text ([ftp=ftp://url]text[/ftp])
        '#\[ftp\]\n?(ftp://.*?)\[/ftp\]#is', // FTP link ([ftp]ftp://url[/ftp])
        '#\[ftp=(.*?)\]\n?(.*?)\[/ftp\]#is', // FTP link with descriptive text ([ftp=ftp://url]text[/ftp])
        '#\[ftp\]\n?(.*?)\[/ftp\]#is', // FTP link ([ftp]http://url[/ftp])
        '#\[table\]\n?(.*?)\[/table\]\n?#is', // Table ([table]text[/table])
        '#\[tr\]\n?(.*?)\[/tr\]\n?#is', // Table row ([tr]text[/tr])
        '#\[td\]\n?(.*?)\[/td\]\n?#is', // Table column ([td]text[/td])
        '#\[sup\]\n?(.*?)\[/sup\]#is', // Superscript ([sup]text[/sup])
        '#\[sub\]\n?(.*?)\[/sub\]#is', // Subscript ([sub]text[/sub])
        '#\[tt\]\n?(.*?)\[/tt\]#is', // Teletype ([tt]text[/tt])
        '#\[code\]\n?(.*?)\[/code\]#is', // Monospaced code [code]text[/code])
        // Add support to add a specific person to quote
        //   Ex. "Username said: " [quote=Username][/quote]
        '#\[quote\]\n?(.*?)\[/quote\]\n?#is', // Quote ([quote]text[/quote])
        '#\[list\]\n?(.*?)\[/list\]#is',
        '#\[list=ordered\]\n?(.*?)\[/list\]#is',
        '#\[li\]\n?(.*?)\[/li\]#is',
    ];

    // The matching array of strings to replace matches with
    $format_replace = [
        '<b>$1</b>',
        '<i>$1</i>',
        '<span style="text-decoration: underline;">$1</span>',
        '<span style="text-decoration: line-through;">$1</span>',
        '<span style="background-color: $1;">$2</span>',
        '<span style="text-shadow: $4 $1px $2px $3px;">$5</span>',// Shadow goes here...
        // might need a special area for shadow due to it needing some process
        '<marquee>$1</marquee>',
        '<pre>$1</pre>',
        '<div style="text-align: left;">$1</div>',
        '<div style="text-align: center;">$1</div>',
        '<div style="text-align: right;">$1</div>',
        '<hr>',
        '<span style="font-size: $1px;">$2</span>',
        '<span style="font-family: $1;">$2</span>',// Font Face goes here...
        // need a special algorithm to check fonts
        '<span style="color: $1;">$2</span>',
        '<object type="application/x-shockwave-flash" data="$3" width="$1" height="$2">'.
          '<param name="movie" value="$3" />'.
          '<param name="wmode" value="transparent">'.
        '</object>',
        '<img class="post-image" src="$2" alt="$1" />',
        '<img class="post-image" src="$1" />',
        '<img class="post-image" src="$3" width="$1" height="$2" />',
        '<img class="post-image" src="$4" width="$1" height="$2" alt="$3" />',
        '<img class="post-image $1" src="$5" width="$2" height="$3" alt="$4" />',
        '<a href="$1">$2</a>',
        '<a href="$1">$1</a>',
        '<a href="http://$1">$2</a>',
        '<a href="http://$1">$1</a>',
        '<a href="mailto:$1">$1</a>',
        '<a href="$1">$2</a>',
        '<a href="$1">$1</a>',
        '<a href="ftp://$1">$2</a>',
        '<a href="ftp://$1">$1</a>',
        '<table>$1</table>',
        '<tr>$1</tr>',
        '<td>$1</td>',
        '<sup>$1</sup>',
        '<sub>$1</sub>',
        '<tt>$1</tt>',
        '<code>$1</code>',
        '<blockquote>$1</blockquote>',
        '<ul>$1</ul>',
        '<ol>$1</ol>',
        '<li>$1</li>',
    ];

    // Perform the actual conversion
    $str = preg_replace($format_search, $format_replace, $str);

    //Smileys!
    $str = smiley_replace($str);

    // Convert line breaks in the <br /> tag
    $str = nl2br($str);

    return $str;
}

function smiley_replace($str)
{
    $newStr = $str;

    //Smileys!
    $smilies['plaintext'] = [
        ' :)',
        ' :~',
        ' :B',
        ' :|',
        ' 8-)',
        ' :&lt;',
        ' :$',
        ' :X',
        ' :Z',
        ' :\'(',
        ' :-|',
        ' :@',
        ' :P',
        ' :D',
        ' :O',
        ' :(',
        ' :+',
        ' --b',
        ' :Q',
        ' :T',
        ' ;P',
        ' ;-D',
        ' ;d',
        ' ;o',
        ' :g',
        ' |-)',
        ' :!',
        ' :L',
        ' :&gt;',
        ' :;',
        ' ;f',
        ' :-S',
        ' ???',
        ' ;x',
        ' ;@',
        ' :8',
        ' ;!',
        ' !!!',
        ' x_x',
        ' ::bye::',
        ' ::wipe::',
        ' ::dig::',
        ' ::handclap::',
        ' &amp;-(',
        ' B-)',
        ' &lt;@',
        ' @&gt;',
        ' :-O',
        ' &gt;-|',
        ' P-(',
        ' :\'|',
        ' X-)',
        ' :*',
        ' @x',
        ' 8*',
        ' ::cleaver::',
        ' ::watermelon::',
        ' ::beer::',
        ' ::basketball::',
        ' ::pingpong::',
        ' ::coffee::',
        ' ::rice::',
        ' ::pig::',
        ' ::rose::',
        ' ::wilt::',
        ' ::kiss::',
        ' ::heart::',
        ' ::break::',
        ' ::cake::',
        ' ::lightning::',
        ' ::bomb::',
        ' ::dagger::',
        ' ::soccer::',
        ' ::ladybug::',
        ' ::poop::',
        ' ::moon::',
        ' ::sun::',
        ' ::gift::',
        ' ::hug::',
        ' ::thumbsup::',
        ' ::thumbsdown::',
        ' ::shake::',
        ' ::victory::',
        ' ::admire::',
        ' ::beckon::',
        ' ::fist::',
        ' ::pinky::',
        ' ::love::',
        ' ::no::',
        ' ::ok::',
        ' ::lovebirds::',
        ' ::inlove::',
        ' ::waddle::',
        ' ::sit::',
        ' ::yell::',
        ' ::twirl::',
        ' ::bow::',
        ' ::turnaway::',
        ' ::jump::',
        ' ::surrender::',
        ' ::hooray::',
        ' ::hiphop::',
        ' ::blowkiss::',
        ' ::fightgirl::',
        ' ::fightboy::',
    ];

    $smilies['htmltext'] = [
        '<img class="emote" src="/img/emote/qq/1.gif" width="24" height="24" alt=" :)" />',
        '<img class="emote" src="/img/emote/qq/2.gif" width="24" height="24" alt=" :~" />',
        '<img class="emote" src="/img/emote/qq/3.gif" width="24" height="24" alt=" :B" />',
        '<img class="emote" src="/img/emote/qq/4.gif" width="24" height="24" alt=" :|" />',
        '<img class="emote" src="/img/emote/qq/5.gif" width="24" height="24" alt=" 8-)" />',
        '<img class="emote" src="/img/emote/qq/6.gif" width="24" height="24" alt=" :&lt;" />',
        '<img class="emote" src="/img/emote/qq/7.gif" width="24" height="24" alt=" :$" />',
        '<img class="emote" src="/img/emote/qq/8.gif" width="24" height="24" alt=" :X" />',
        '<img class="emote" src="/img/emote/qq/9.gif" width="24" height="24" alt=" :Z" />',
        '<img class="emote" src="/img/emote/qq/10.gif" width="24" height="24" alt=" :\'(" />',
        '<img class="emote" src="/img/emote/qq/11.gif" width="24" height="24" alt=" :-|" />',
        '<img class="emote" src="/img/emote/qq/12.gif" width="24" height="24" alt=" :@" />',
        '<img class="emote" src="/img/emote/qq/13.gif" width="24" height="24" alt=" :P" />',
        '<img class="emote" src="/img/emote/qq/14.gif" width="24" height="24" alt=" :D" />',
        '<img class="emote" src="/img/emote/qq/15.gif" width="24" height="24" alt=" :O" />',
        '<img class="emote" src="/img/emote/qq/16.gif" width="24" height="24" alt=" :(" />',
        '<img class="emote" src="/img/emote/qq/17.gif" width="24" height="24" alt=" :+" />',
        '<img class="emote" src="/img/emote/qq/18.gif" width="24" height="24" alt=" --b" />',
        '<img class="emote" src="/img/emote/qq/19.gif" width="24" height="24" alt=" :Q" />',
        '<img class="emote" src="/img/emote/qq/20.gif" width="24" height="24" alt=" :T" />',
        '<img class="emote" src="/img/emote/qq/21.gif" width="24" height="24" alt=" ;P" />',
        '<img class="emote" src="/img/emote/qq/22.gif" width="24" height="24" alt=" ;-D" />',
        '<img class="emote" src="/img/emote/qq/23.gif" width="24" height="24" alt=" ;d" />',
        '<img class="emote" src="/img/emote/qq/24.gif" width="24" height="24" alt=" ;o" />',
        '<img class="emote" src="/img/emote/qq/25.gif" width="24" height="24" alt=" :g" />',
        '<img class="emote" src="/img/emote/qq/26.gif" width="24" height="24" alt=" |-)" />',
        '<img class="emote" src="/img/emote/qq/27.gif" width="24" height="24" alt=" :!" />',
        '<img class="emote" src="/img/emote/qq/28.gif" width="24" height="24" alt=" :L" />',
        '<img class="emote" src="/img/emote/qq/29.gif" width="24" height="24" alt=" :&gt;" />',
        '<img class="emote" src="/img/emote/qq/30.gif" width="24" height="24" alt=" :;" />',
        '<img class="emote" src="/img/emote/qq/31.gif" width="24" height="24" alt=" ;f" />',
        '<img class="emote" src="/img/emote/qq/32.gif" width="24" height="24" alt=" :-S" />',
        '<img class="emote" src="/img/emote/qq/33.gif" width="24" height="24" alt=" ???" />',
        '<img class="emote" src="/img/emote/qq/34.gif" width="24" height="24" alt=" ;x" />',
        '<img class="emote" src="/img/emote/qq/35.gif" width="24" height="24" alt=" ;@" />',
        '<img class="emote" src="/img/emote/qq/36.gif" width="24" height="24" alt=" :8" />',
        '<img class="emote" src="/img/emote/qq/37.gif" width="24" height="24" alt=" ;!" />',
        '<img class="emote" src="/img/emote/qq/38.gif" width="24" height="24" alt=" !!!" />',
        '<img class="emote" src="/img/emote/qq/39.gif" width="24" height="24" alt=" x_x" />',
        '<img class="emote" src="/img/emote/qq/40.gif" width="24" height="24" alt=" ::bye::" />',
        '<img class="emote" src="/img/emote/qq/41.gif" width="24" height="24" alt="　::wipe::" />',
        '<img class="emote" src="/img/emote/qq/42.gif" width="24" height="24" alt=" ::dig::" />',
        '<img class="emote" src="/img/emote/qq/43.gif" width="24" height="24" alt=" ::handclap::" />',
        '<img class="emote" src="/img/emote/qq/44.gif" width="24" height="24" alt=" &amp;-(" />',
        '<img class="emote" src="/img/emote/qq/45.gif" width="24" height="24" alt=" B-)" />',
        '<img class="emote" src="/img/emote/qq/46.gif" width="24" height="24" alt=" &lt;@" />',
        '<img class="emote" src="/img/emote/qq/47.gif" width="24" height="24" alt=" @&gt;" />',
        '<img class="emote" src="/img/emote/qq/48.gif" width="24" height="24" alt=" :-O" />',
        '<img class="emote" src="/img/emote/qq/49.gif" width="24" height="24" alt=" &gt;-|" />',
        '<img class="emote" src="/img/emote/qq/50.gif" width="24" height="24" alt=" P-(" />',
        '<img class="emote" src="/img/emote/qq/51.gif" width="24" height="24" alt=" :\'|" />',
        '<img class="emote" src="/img/emote/qq/52.gif" width="24" height="24" alt=" X-)" />',
        '<img class="emote" src="/img/emote/qq/53.gif" width="24" height="24" alt=" :*" />',
        '<img class="emote" src="/img/emote/qq/54.gif" width="24" height="24" alt=" @x" />',
        '<img class="emote" src="/img/emote/qq/55.gif" width="24" height="24" alt=" 8*" />',
        '<img class="emote" src="/img/emote/qq/56.gif" width="24" height="24" alt=" ::cleaver::" />',
        '<img class="emote" src="/img/emote/qq/57.gif" width="24" height="24" alt=" ::watermelon::" />',
        '<img class="emote" src="/img/emote/qq/58.gif" width="24" height="24" alt=" ::beer::" />',
        '<img class="emote" src="/img/emote/qq/59.gif" width="24" height="24" alt=" ::basketball::" />',
        '<img class="emote" src="/img/emote/qq/60.gif" width="24" height="24" alt=" ::pingpong::" />',
        '<img class="emote" src="/img/emote/qq/61.gif" width="24" height="24" alt=" ::coffee::" />',
        '<img class="emote" src="/img/emote/qq/62.gif" width="24" height="24" alt=" ::rice::" />',
        '<img class="emote" src="/img/emote/qq/63.gif" width="24" height="24" alt=" ::pig::" />',
        '<img class="emote" src="/img/emote/qq/64.gif" width="24" height="24" alt=" ::rose::" />',
        '<img class="emote" src="/img/emote/qq/65.gif" width="24" height="24" alt=" ::wilt::" />',
        '<img class="emote" src="/img/emote/qq/66.gif" width="24" height="24" alt=" ::kiss::" />',
        '<img class="emote" src="/img/emote/qq/67.gif" width="24" height="24" alt=" ::heart::" />',
        '<img class="emote" src="/img/emote/qq/68.gif" width="24" height="24" alt=" ::break::" />',
        '<img class="emote" src="/img/emote/qq/69.gif" width="24" height="24" alt=" ::cake::" />',
        '<img class="emote" src="/img/emote/qq/70.gif" width="24" height="24" alt=" ::lightning::" />',
        '<img class="emote" src="/img/emote/qq/71.gif" width="24" height="24" alt=" ::bomb::" />',
        '<img class="emote" src="/img/emote/qq/72.gif" width="24" height="24" alt=" ::dagger::" />',
        '<img class="emote" src="/img/emote/qq/73.gif" width="24" height="24" alt=" ::soccer::" />',
        '<img class="emote" src="/img/emote/qq/74.gif" width="24" height="24" alt=" ::ladybug::" />',
        '<img class="emote" src="/img/emote/qq/75.gif" width="24" height="24" alt=" ::poop::" />',
        '<img class="emote" src="/img/emote/qq/76.gif" width="24" height="24" alt=" ::moon::" />',
        '<img class="emote" src="/img/emote/qq/77.gif" width="24" height="24" alt=" ::sun::" />',
        '<img class="emote" src="/img/emote/qq/78.gif" width="24" height="24" alt=" ::gift::" />',
        '<img class="emote" src="/img/emote/qq/79.gif" width="24" height="24" alt=" ::hug::" />',
        '<img class="emote" src="/img/emote/qq/80.gif" width="24" height="24" alt=" ::thumbsup::" />',
        '<img class="emote" src="/img/emote/qq/81.gif" width="24" height="24" alt=" ::thumbsdown::" />',
        '<img class="emote" src="/img/emote/qq/82.gif" width="24" height="24" alt=" ::shake::" />',
        '<img class="emote" src="/img/emote/qq/83.gif" width="24" height="24" alt=" ::victory::" />',
        '<img class="emote" src="/img/emote/qq/84.gif" width="24" height="24" alt=" ::admire::" />',
        '<img class="emote" src="/img/emote/qq/85.gif" width="24" height="24" alt=" ::beckon::" />',
        '<img class="emote" src="/img/emote/qq/86.gif" width="24" height="24" alt=" ::fist::" />',
        '<img class="emote" src="/img/emote/qq/87.gif" width="24" height="24" alt=" ::pinky::" />',
        '<img class="emote" src="/img/emote/qq/88.gif" width="24" height="24" alt=" ::love::" />',
        '<img class="emote" src="/img/emote/qq/89.gif" width="24" height="24" alt=" ::no::" />',
        '<img class="emote" src="/img/emote/qq/90.gif" width="24" height="24" alt=" ::ok::" />',
        '<img class="emote" src="/img/emote/qq/91.gif" width="24" height="24" alt=" ::lovebirds::" />',
        '<img class="emote" src="/img/emote/qq/92.gif" width="24" height="24" alt=" ::inlove::" />',
        '<img class="emote" src="/img/emote/qq/93.gif" width="24" height="24" alt=" ::waddle::" />',
        '<img class="emote" src="/img/emote/qq/94.gif" width="24" height="24" alt=" ::sit::" />',
        '<img class="emote" src="/img/emote/qq/95.gif" width="24" height="24" alt=" ::yell::" />',
        '<img class="emote" src="/img/emote/qq/96.gif" width="24" height="24" alt=" ::twirl::" />',
        '<img class="emote" src="/img/emote/qq/97.gif" width="24" height="24" alt=" ::bow::" />',
        '<img class="emote" src="/img/emote/qq/98.gif" width="24" height="24" alt=" ::turnaway::" />',
        '<img class="emote" src="/img/emote/qq/99.gif" width="24" height="24" alt=" ::jump::" />',
        '<img class="emote" src="/img/emote/qq/100.gif" width="24" height="24" alt=" ::surrender::" />',
        '<img class="emote" src="/img/emote/qq/101.gif" width="24" height="24" alt=" ::hooray::" />',
        '<img class="emote" src="/img/emote/qq/102.gif" width="24" height="24" alt=" ::hiphop::" />',
        '<img class="emote" src="/img/emote/qq/103.gif" width="24" height="24" alt=" ::blowkiss::" />',
        '<img class="emote" src="/img/emote/qq/104.gif" width="24" height="24" alt=" ::fightgirl::" />',
        '<img class="emote" src="/img/emote/qq/105.gif" width="24" height="24" alt=" ::fightboy::" />',
    ];

    $newStr = str_replace($smilies['plaintext'], $smilies['htmltext'], $newStr);

    return $newStr;
}

function showPage($include = null, $css = null, $js = null, $message)
{
    include 'layout/top-variable.php';
    echo $message;
    include 'layout/bottom.php';
}

function getURL($url, $query)
{
    $queryString = '/';

    if (isset($query['year'])) {
        $queryString .= $query['year'].'/';
        if (isset($query['month'])) {
            $queryString .= $query['month'].'/';
            if (isset($query['day'])) {
                $queryString .= $query['day'].'/';
            }
        }
    } elseif (isset($query['page'])) {
        $queryString .= $query['page'].'/';
    } elseif (isset($_GET['tags'])) {
        $queryString .= 'tags/'.$query['tags'].'/';
    } elseif (isset($_GET['post'])) {
        $queryString .= 'post/'.$query['post'].'/';
    }

    return $url.$queryString;
}
