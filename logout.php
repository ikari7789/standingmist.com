<?php

@session_start();

function logout()
{
    if ($_SESSION['loggedIn']) {
        unset($_SESSION['loggedIn']);
        unset($_SESSION['user']);
        $message = '<div class="box message">You\'ve been successfully logged out</div>';

        return $message;
    }

    return '<div class="error">You aren\'t logged in... Why are you trying to log out?</div>';
}

include_once 'lib/session.php';
$message = logout();
define('TITLE', 'Standing in the Mist - 霞で立ってる - Logout');
if (isset($_GET['redirect'])) {
    $redirectUrl = $_GET['redirect'];
} else {
    $redirectUrl = BASE_URL;
}
$metaInfo = '<meta http-equiv="refresh" content="2;url='.$redirectUrl.'">';
include 'inc/header.php';
?>

<body>
<?php include 'inc/banner.php'; ?>
<div id="body">
    <div class="border">
        <table id="container">
            <tr>
                <td id="leftcol">
                    <?php echo $message; ?>
                </td>
                <td class="spacer"></td>
                <td id="rightcol">
                    <?php include 'inc/rightcol.php'; ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<?php include 'inc/footer.php'; ?>
</body>
</html>
