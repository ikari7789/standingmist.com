<div class="searchBox">
    <a href="" class="close">[X]</a>
    <span class="title">Search Results</span>
    <hr>
<?php
if (isset($_GET['search'])) {
    include_once 'lib/constants.php';
    include_once 'lib/mysqli.php';
    include_once 'lib/post.php';

    $postMan->search($_GET['search']);
}
?>
</div>
