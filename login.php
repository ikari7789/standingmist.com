<?php

/**
 * Login System.
 */
class LoginSystem
{
    private $db;

    public function __construct()
    {
        global $db;
        $this->db = $db;
    }

    public function displayLoginScreen()
    {
        if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true) {
            ?>
<div class="login">
    <div class="loggedIn">
        <p>Logged in as: <b><?php echo $_SESSION['user']['username'];
            ?></b></p>
        <a href="logout?redirect=<?php echo $_SERVER['REQUEST_URI'];
            ?>">Logout</a>
    </div>
</div>
<?php

        } else {
            ?>
<div class="login">
    <form id="login" action="login" method="post">
        <div class="left-align">
            <label for="username">Username:</label>
            <input id="username" name="username" type="text" />
            <label for="password">Password:</label>
            <input id="password" name="password" type="password" />
            <input type="hidden" id="redirect" name="redirect" value="<?php echo $_SERVER['REQUEST_URI'];
            ?>" />
        </div>
        <button type="submit" value="Login">Login</button>
    </form>
</div>
<?php

        }
    }

    /**
     * Login the user, if the user exists and the password is right,
     * set the user as logged in, else, set them as logged out.
     */
    public function loginUser($loginVals)
    {
        $username = $loginVals['username'];
        $md5Pass = '';

        if (strlen($loginVals['password']) == 0) {
            return '<div class="error">Please enter a password!</div>';
        } else {
            $md5Pass = $this->encryptPassword($loginVals['password']);
        }

        if ($this->checkUser($username, $md5Pass)) {
            $_SESSION['loggedIn'] = true;
            $_SESSION['user']['username'] = $username;
            $message = '<div class="box message">You have successfully been logged in as <b>'.$_SESSION['user']['username'].'</b><div>';
        } else {
            $_SESSION['loggedIn'] = false;
            $message = '<div class="error">Login failed... please try again!</div>';
        }

        return $message;
    }

    public function encryptPassword($password)
    {
        return md5($password);
    }

    public function checkUser($username, $password)
    {
        $query = sprintf(
            'SELECT * FROM '.$this->db->prefix."users WHERE username='%s';",
            $this->db->escape($username)
        );
        $result = $this->db->query($query) or die($this->db->error());

        if (! is_bool($result)) {
            $_SESSION['user'] = mysqli_fetch_array($result, MYSQLI_ASSOC);
            if ($this->db->numRows($result) == 1) {
                if ($password != '') {
                    if ($_SESSION['user']['password'] == $password) {
                        $query = sprintf(
                            "UPDATE %susers SET IP = '%s', lastAccess = NOW() WHERE username = '%s'",
                            $this->db->escape($this->db->prefix),
                            $this->db->escape($_SERVER['REMOTE_ADDR']),
                            $this->db->escape($_SESSION['user']['username'])
                        );
                        $this->db->query($query);

                        return true;
                    }
                }
            }
        }

        return false;
    }
}

@session_start();

$loginSystem = new LoginSystem();

if (! isset($_SESSION['loggedIn'])) {
    $_SESSION['loggedIn'] = false;
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['username']) && isset($_POST['password'])) {
    include_once 'lib/session.php';
    define('TITLE', 'Standing in the Mist - 霞で立ってる - Login');
    if (isset($_POST['redirect'])) {
        $redirectUrl = $_POST['redirect'];
    } else {
        $redirectUrl = BASE_URL;
    }
    $metaInfo = '<meta http-equiv="refresh" content="2;url='.$redirectUrl.'">';
    include 'inc/header.php';
    $message = $loginSystem->loginUser($_POST);
    ?>
<body>
<?php include 'inc/banner.php'; ?>
<div id="body">
<div class="border">
<table id="container">
    <tr>
        <td id="leftcol">
            <?php echo $message;
    ?>
        </td>
        <td class="spacer"></td>
        <td id="rightcol">
            <?php include 'inc/rightcol.php';
    ?>
        </td>
    </tr>
</table>
</div>
</div>
<?php include 'inc/footer.php';
    ?>
</body>
</html>
<?php

}
?>
