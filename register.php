<?php

/**
 * Registration page for new users.
 */
class Registration
{
    public function displayRegistrationForm()
    {
        ?>
<form action="" method="post" id="registration">
    <ul>
        <li>
            <label for="name">Username:</label>
            <input type="text" id="name" name="name" size="32" />
            <span class="error"></span>
        </li>
        <li>
            <label for="pass1">Password:</label>
            <input type="password" id="pass1" name="pass1" />
            <span class="error"></span>
        </li>
        <li>
            <label for="pass2">Password (Confirm):</label>
            <input type="text" id="pass2" name="pass2" />
            <span class="error"></span>
        </li>
        <li>
            <label for="email1">Email:</label>
            <input type="text" id="email1" name="email1" />
            <span class="error"></span>
        </li>
        <li>
            <label for="email2">Email (Confirm):</label>
            <input type="text" id="email2" name="email2" />
            <span class="error"></span>
        </li>
        <li>
            <button type="submit">Register</button>
            <button type="reset">Reset</button>
        </li>
    </ul>
</form>
<?php

    }
}

$registrationMon = new Registration();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $registrationMon->processRegistration($_POST);
} else {
    $registrationMon->displayRegistrationForm();
}
