SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `old_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `commentID` int(11) NOT NULL AUTO_INCREMENT,
  `post` int(11) NOT NULL,
  `user` int(32) NOT NULL,
  `time` datetime NOT NULL,
  `comment` longtext NOT NULL,
  PRIMARY KEY (`commentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `infobox`
--

CREATE TABLE IF NOT EXISTS `infobox` (
  `title` varchar(50) NOT NULL,
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jcomments`
--

CREATE TABLE IF NOT EXISTS `jcomments` (
  `commentID` int(11) NOT NULL AUTO_INCREMENT,
  `post` int(11) NOT NULL,
  `user` int(32) NOT NULL,
  `time` datetime NOT NULL,
  `comment` longtext NOT NULL,
  PRIMARY KEY (`commentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jposts`
--

CREATE TABLE IF NOT EXISTS `jposts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniqueTitle` varchar(140) CHARACTER SET utf8 NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `author` varchar(32) CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL,
  `lastUpdated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `content` longtext CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqueTitle` (`uniqueTitle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jtags`
--

CREATE TABLE IF NOT EXISTS `jtags` (
  `postID` int(11) NOT NULL,
  `tag` varchar(25) NOT NULL,
  PRIMARY KEY (`postID`,`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniqueTitle` varchar(140) CHARACTER SET utf8 NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `author` varchar(32) CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL,
  `lastUpdated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `content` longtext CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqueTitle` (`uniqueTitle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `postID` int(11) NOT NULL,
  `tag` varchar(25) NOT NULL,
  PRIMARY KEY (`postID`,`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL,
  `homepage` varchar(75) NOT NULL,
  `level` int(11) NOT NULL,
  `IP` varchar(15) NOT NULL,
  `joinDate` date NOT NULL,
  `lastAccess` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;