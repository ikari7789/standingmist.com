<?php

@session_start();

function editForm($formLink)
{
    if (isset($_GET['update'])) {
        include_once 'lib/constants.php';
        include_once 'lib/mysqli.php';
        if ((isset($_GET['japanese']) && $_GET['japanese']) || (isset($_POST['japanese']) && $_POST['japanese'])) {
            $jpost = 'j';
        } else {
            $jpost = '';
        }
        $query = sprintf(
            'SELECT * FROM %s%sposts WHERE uniqueTitle = "%s";',
            $db->escape($db->prefix),
            $jpost,
            $db->escape($_GET['post'])
        );
        $result = $db->query($query) or die($db->error());
        $editInfo = $db->fetchArray($result, MYSQLI_ASSOC);

        $query = sprintf(
            'SELECT tag FROM %s%stags t WHERE postID = %d;',
            $db->escape($db->prefix),
            $jpost,
            $editInfo['id']
        );
        $result = $db->query($query);
        $editInfo['tags'] = '';
        if (! is_bool($result) && $db->numRows($result) > 0) {
            while ($tag = $db->fetchArray($result, MYSQLI_ASSOC)) {
                $editInfo['tags'] .= $tag['tag'].' ';
            }
            $editInfo['tags'] = substr($editInfo['tags'], 0, strlen($editInfo['tags']) - 1);
        }
    }
    ?>
<div class="box" id="previewBox" style="display: none;"></div>
<div class="box" id="editBox">
    <form action="edit" method="post" id="edit-form" class="edit-form">
        <input type="hidden" id="postID" value="<?php if (isset($_GET['update'])) {
    echo $editInfo['id'];
} else {
    echo '0';
}
    ?>" />
        <ul>
            <li>
                <label for="title">Title:</label>
                <input class="information" type="text" id="title" name="title" <?php if (isset($_GET['update'])) {
    echo 'value="'.$editInfo['title'].'"';
}
    ?> />
            </li>
            <li>
                <label for="tags">Tags:</label>
                <input class="information" type="text" id="tags" name="tags" <?php if (isset($_GET['update'])) {
    echo 'value="'.$editInfo['tags'].'"';
}
    ?> />
            </li>
        </ul>
        <a href="" class="cancel">[X]</a>
        <div class="edit">
            <div class="row">
                <a href="" class="edit-item surround"><img src="/img/edit/bold.png" width="23" height="22" alt="bold" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/italicize.png" width="23" height="22" alt="italics" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/underline.png" width="23" height="22" alt="underline" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/strike.png" width="23" height="22" alt="strike" /></a>
                <div class="spacer"></div>
                <a href="" class="edit-item surround"><img src="/img/edit/glow.png" width="23" height="22" alt="glow" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/shadow.png" width="23" height="22" alt="shadow" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/move.png" width="23" height="22" alt="move" /></a>
                <div class="spacer"></div>
                <a href="" class="edit-item surround"><img src="/img/edit/left.png" width="23" height="22" alt="left" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/center.png" width="23" height="22" alt="center" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/right.png" width="23" height="22" alt="right" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/pre.png" width="23" height="22" alt="pre" /></a>
                <div class="spacer"></div>
                <a href="" class="edit-item replace"><img src="/img/edit/hr.png" width="23" height="22" alt="[hr]" /></a>
                <div class="spacer"></div>
                <a href="" class="edit-item surround"><img src="/img/edit/size.png" width="23" height="22" alt="size" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/face.png" width="23" height="22" alt="face" /></a>
                <select id="fontColor" name="fontColor">
                    <option value="">Select Color...</option>
                    <option value="black">Black</option>
                    <option value="red">Red</option>
                    <option value="yellow">Yellow</option>
                    <option value="pink">Pink</option>
                    <option value="green">Green</option>
                    <option value="orange">Orange</option>
                    <option value="purple">Purple</option>
                    <option value="blue">Blue</option>
                    <option value="beige">Beige</option>
                    <option value="brown">Brown</option>
                    <option value="teal">Teal</option>
                    <option value="navy">Navy</option>
                    <option value="maroon">Maroon</option>
                    <option value="lime">Lime Green</option>
                </select>
            </div>
            <div class="row">
                <div class="edit-item emoticon">
                    <img src="/img/emote/smiley.gif" width="15" height="15" alt="smiley" />
                    <div class="inner-box">
                        <div class="inner-row">
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/1.jpg" width="28" height="28" alt=" :)" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/2.jpg" width="28" height="28" alt=" :~" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/3.jpg" width="28" height="28" alt=" :B" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/4.jpg" width="28" height="28" alt=" :|" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/5.jpg" width="28" height="28" alt=" 8-)" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/6.jpg" width="28" height="28" alt=" :&lt;" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/7.jpg" width="28" height="28" alt=" :$" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/8.jpg" width="28" height="28" alt=" :X" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/9.jpg" width="28" height="28" alt=" :Z" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/10.jpg" width="28" height="28" alt=" :'(" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/11.jpg" width="28" height="28" alt=" :-|" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/12.jpg" width="28" height="28" alt=" :@" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/13.jpg" width="28" height="28" alt=" :P" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/14.jpg" width="28" height="28" alt=" :D" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/15.jpg" width="28" height="28" alt=" :O" /></a>
                        </div>
                        <div class="inner-row">
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/16.jpg" width="28" height="28" alt=" :(" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/17.jpg" width="28" height="28" alt=" :+" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/18.jpg" width="28" height="28" alt=" --b" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/19.jpg" width="28" height="28" alt=" :Q" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/20.jpg" width="28" height="28" alt=" :T" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/21.jpg" width="28" height="28" alt=" ;P" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/22.jpg" width="28" height="28" alt=" ;-D" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/23.jpg" width="28" height="28" alt=" ;d" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/24.jpg" width="28" height="28" alt=" ;o" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/25.jpg" width="28" height="28" alt=" :g" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/26.jpg" width="28" height="28" alt=" |-)" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/27.jpg" width="28" height="28" alt=" :!" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/28.jpg" width="28" height="28" alt=" :L" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/29.jpg" width="28" height="28" alt=" :>" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/30.jpg" width="28" height="28" alt=" :;" /></a>
                        </div>
                        <div class="inner-row">
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/31.jpg" width="28" height="28" alt=" ;f" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/32.jpg" width="28" height="28" alt=" :-S" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/33.jpg" width="28" height="28" alt=" ???" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/34.jpg" width="28" height="28" alt=" ;x" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/35.jpg" width="28" height="28" alt=" ;@" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/36.jpg" width="28" height="28" alt=" :8" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/37.jpg" width="28" height="28" alt=" ;!" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/38.jpg" width="28" height="28" alt=" !!!" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/39.jpg" width="28" height="28" alt=" x_x" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/40.jpg" width="28" height="28" alt=" ::bye::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/41.jpg" width="28" height="28" alt="　::wipe::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/42.jpg" width="28" height="28" alt=" ::dig::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/43.jpg" width="28" height="28" alt=" ::handclap::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/44.jpg" width="28" height="28" alt=" &amp;-(" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/45.jpg" width="28" height="28" alt=" B-)" /></a>
                        </div>
                        <div class="inner-row">
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/46.jpg" width="28" height="28" alt=" <@" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/47.jpg" width="28" height="28" alt=" @>" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/48.jpg" width="28" height="28" alt=" :-O" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/49.jpg" width="28" height="28" alt=" >-|" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/50.jpg" width="28" height="28" alt=" P-(" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/51.jpg" width="28" height="28" alt=" :'|" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/52.jpg" width="28" height="28" alt=" X-)" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/53.jpg" width="28" height="28" alt=" :*" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/54.jpg" width="28" height="28" alt=" @x" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/55.jpg" width="28" height="28" alt=" 8*" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/56.jpg" width="28" height="28" alt=" ::cleaver::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/57.jpg" width="28" height="28" alt=" ::watermelon::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/58.jpg" width="28" height="28" alt=" ::beer::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/59.jpg" width="28" height="28" alt=" ::basketball::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/60.jpg" width="28" height="28" alt=" ::pingpong::" /></a>
                        </div>
                        <div class="inner-row">
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/61.jpg" width="28" height="28" alt=" ::coffee::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/62.jpg" width="28" height="28" alt=" ::rice::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/63.jpg" width="28" height="28" alt=" ::pig::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/64.jpg" width="28" height="28" alt=" ::rose::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/65.jpg" width="28" height="28" alt=" ::wilt::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/66.jpg" width="28" height="28" alt=" ::kiss::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/67.jpg" width="28" height="28" alt=" ::heart::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/68.jpg" width="28" height="28" alt=" ::break::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/69.jpg" width="28" height="28" alt=" ::cake::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/70.jpg" width="28" height="28" alt=" ::lightning::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/71.jpg" width="28" height="28" alt=" ::bomb::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/72.jpg" width="28" height="28" alt=" ::dagger::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/73.jpg" width="28" height="28" alt=" ::soccer::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/74.jpg" width="28" height="28" alt=" ::ladybug::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/75.jpg" width="28" height="28" alt=" ::poop::" /></a>
                        </div>
                        <div class="inner-row">
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/76.jpg" width="28" height="28" alt=" ::moon::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/77.jpg" width="28" height="28" alt=" ::sun::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/78.jpg" width="28" height="28" alt=" ::gift::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/79.jpg" width="28" height="28" alt=" ::hug::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/80.jpg" width="28" height="28" alt=" ::thumbsup::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/81.jpg" width="28" height="28" alt=" ::thumbsdown::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/82.jpg" width="28" height="28" alt=" ::shake::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/83.jpg" width="28" height="28" alt=" ::victory::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/84.jpg" width="28" height="28" alt=" ::admire::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/85.jpg" width="28" height="28" alt=" ::beckon::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/86.jpg" width="28" height="28" alt=" ::fist::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/87.jpg" width="28" height="28" alt=" ::pinky::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/88.jpg" width="28" height="28" alt=" ::love::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/89.jpg" width="28" height="28" alt=" ::no::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/90.jpg" width="28" height="28" alt=" ::ok::" /></a>
                        </div>
                        <div class="inner-row">
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/91.jpg" width="28" height="28" alt=" ::lovebirds::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/92.jpg" width="28" height="28" alt=" ::inlove::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/93.jpg" width="28" height="28" alt=" ::waddle::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/94.jpg" width="28" height="28" alt=" ::sit::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/95.jpg" width="28" height="28" alt=" ::yell::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/96.jpg" width="28" height="28" alt=" ::twirl::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/97.jpg" width="28" height="28" alt=" ::bow::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/98.jpg" width="28" height="28" alt=" ::turnaway::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/99.jpg" width="28" height="28" alt=" ::jump::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/100.jpg" width="28" height="28" alt=" ::surrender::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/101.jpg" width="28" height="28" alt=" ::hooray::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/102.jpg" width="28" height="28" alt=" ::hiphop::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/103.jpg" width="28" height="28" alt=" ::blowkiss::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/104.jpg" width="28" height="28" alt=" ::fightgirl::" /></a>
                            <a href="" class="inner-edit-item replace"><img class="emote" src="/img/emote/qq/still/105.jpg" width="28" height="28" alt=" ::fightboy::" /></a>
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <a href="" class="edit-item surround"><img src="/img/edit/flash.png" width="23" height="22" alt="flash" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/img.png" width="23" height="22" alt="img" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/url.png" width="23" height="22" alt="url" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/email.png" width="23" height="22" alt="email" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/ftp.png" width="23" height="22" alt="ftp" /></a>
                <div class="spacer"></div>
                <a href="" class="edit-item surround"><img src="/img/edit/table.png" width="23" height="22" alt="table" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/tr.png" width="23" height="22" alt="tr" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/td.png" width="23" height="22" alt="td" /></a>
                <div class="spacer"></div>
                <a href="" class="edit-item surround"><img src="/img/edit/sup.png" width="23" height="22" alt="sup" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/sub.png" width="23" height="22" alt="sub" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/tele.gif" width="23" height="22" alt="tele" /></a>
                <div class="spacer"></div>
                <a href="" class="edit-item surround"><img src="/img/edit/code.png" width="23" height="22" alt="code" /></a>
                <a href="" class="edit-item surround"><img src="/img/edit/quote.png" width="23" height="22" alt="quote" /></a>
                <div class="spacer"></div>
                <a href="" class="edit-item surround"><img src="/img/edit/list.png" width="23" height="22" alt="list" /></a>
            </div>
<?php
    if (isset($_GET['update'])) {
        ?>
<div id="question" style="display: none; cursor: default">
    <h1>Do you really want to delete this post?</h1>
    <input type="button" id="yes" value="Yes" />
    <input type="button" id="no" value="No" />
</div>
<?php

    }
    ?>
            <textarea id="content" name="content"><?php if (isset($_GET['update'])) {
    echo $editInfo['content'];
}
    ?></textarea>
            <button <?php if (isset($_GET['update'])) {
    echo 'id="update"';
} else {
    echo 'id="process"';
}
    ?> type="submit"><?php if (isset($_GET['update'])) {
    echo 'Update';
} else {
    echo 'Post';
}
    ?></button>
            <button id="preview" name="preview">Preview</button>
            <?php if (isset($_GET['update'])) {
    echo '<button id="delete">Delete</button>';
}
    ?>
        </div>
    </form>
</div>
<?php

}

function processPost($values)
{
    include_once 'lib/functions.php';
    include_once 'lib/post.php';
    if (isset($values['preview'])) {
        $post = [
            'id' => -1,
            'uniqueTitle' => $postMan->createUniqueTitle($values['title']),
            'title' => $values['title'],
            'author' => $values['author'],
            'content' => $values['content'],
            'lastUpdate' => '0000-00-00 00:00:00',
            'date' => date('Y-m-d H:i:s', time()),
            'tags' => $values['tags'],
        ];
        $postMan->displayPost($post, false, true);
    } elseif (isset($values['process'])) {
        include_once 'lib/post.php';
        $post = ['title' => $values['title'],
                      'author' => $_SESSION['user']['username'],
                      'tags' => $values['tags'],
                      'content' => $values['content'], ];
        $postMan->addPost($post);
    } elseif (isset($values['update'])) {
        include_once 'lib/post.php';
        $post = [
            'id' => $values['id'],
            'title' => $values['title'],
            'tags' => $values['tags'],
            'content' => $values['content'],
        ];
        $postMan->updatePost($post);
    } elseif (isset($values['delete'])) {
        if ($_SESSION['loggedIn']) {
            include_once 'lib/post.php';
        }
        $postID = $values['id'];
        $postMan->deletePost($postID);
    } else {
        if ($values['title'] == false) {
            echo '<div class="error">MUST HAVE A TITLE!!!</div>';
        }
        if ($values['content'] == false) {
            echo '<div class="error">MUST HAVE A MESSAGE!!!</div>';
        }
    }
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['delete']) || ($_POST['title'] != false && $_POST['content'] != false)) {
        include_once 'lib/constants.php';
        include_once 'lib/mysqli.php';
        processPost($_POST);
    } else {
        if ($_POST['title'] == false) {
            echo '<div class="error">MUST HAVE A TITLE!!!</div>';
        }
        if ($_POST['content'] == false) {
            echo '<div class="error">MUST HAVE A MESSAGE!!!</div>';
        }
    }
} elseif ($_SERVER['REQUEST_METHOD'] == 'GET' && (isset($_GET['newPost']) || isset($_GET['update']))) {
    editForm($_SERVER['PHP_SELF']);
}
?>
